//For Pagination Variable
var limit = 10;
var page = 1;
var loadingAction = 'inactive';

//Getting the webroot
var webroot = function () {
    $hostname = window.location.hostname;
    if ($hostname == 'localhost') {
        return '/microblog3/';
    } else {
        return '/';
    }

}

var getPost = function () {
    return document.querySelector('#post_id').value;
}

var getUser = function () {
    return document.querySelector('#user_id').value;
}

var getSearchWord = function () {
    return document.querySelector('#search_word').value;
}

//Getting Url
var getPostUrl = function () {
    return webroot() + "posts/fetchPost";
}

var getUserPagePostUrl = function () {
    return webroot() + "users/fetchUserPost";
}

var getCommentUrl = function () {
    return webroot() + "comments/fetchComment";
}

var getFollowerUrl = function () {
    return webroot() + "followers/fetchFollowers";
}

var getFollowingUrl = function () {
    return webroot() + "followers/fetchFollowings";
}

var getSearchUrl = function () {
    return webroot() + "posts/fetchSearch";
}

var getUserSearchUrl = function () {
    return webroot() + "users/fetchSearch";
}

var getLikerListUrl = function () {
    return webroot() + "likes/likerList";
}

//Include to Page Function

function init(include) {
    if (include) {
        //for comment
        var coll = document.querySelectorAll(".comment-collapsible");
        comment(coll);
        var editCommentLink = document.querySelectorAll(".edit_comment_link");
        editComment(editCommentLink);
    }
    //for post
    var likesData = document.querySelectorAll(".like-post");
    like(likesData);
    var editPostLink = document.querySelectorAll(".edit_post_link");
    editPost(editPostLink);
    var retweetLink = document.querySelectorAll(".retweet_post_link");
    retweetPost(retweetLink);
    var likers = document.querySelectorAll(".number-of-likes");
    likerList(likers);
    var commentForm = document.querySelectorAll(".save-comment");
    submitPostForm(commentForm);
}

function initIndex() {
    var saveForm = document.querySelectorAll('.save-post');
    submitPostForm(saveForm);

}

function initUser() {
    var userForms =  document.querySelectorAll('.save-user');
    submitForm(userForms);
}


//Only call in userpage

function includeFollow() {
    var foll = document.querySelector('.follow_btn');
    if (foll != null) {
        follow(foll);
    }
}

/*========Logical Functions with Ajax========*/

//for show comment list
function comment(coll) {
    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function (e) {
            e.preventDefault();
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }
}

//for like and unlike function
function like(like) {
    like.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var post_id = this.dataset.post;
                var el = this;
                var url = webroot() + "likes/like";
                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        post_id: post_id,
                    },
                    beforeSend: function (xhr) { // Handle csrf errors
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    },
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            //var likeVar = JSON.parse(data);
                            var likeVar = data;
                            if (likeVar.deleted) {
                                el.innerHTML = 'Like';
                            } else {
                                el.innerHTML = 'Unlike';
                            }
                            if (likeVar.like_count > 1) {
                                document.querySelector('#postcount' + post_id).innerHTML = likeVar.like_count + ' Likes';
                            } else if (likeVar.like_count == 1) {
                                document.querySelector('#postcount' + post_id).innerHTML = likeVar.like_count + ' Like';
                            } else {
                                document.querySelector('#postcount' + post_id).innerHTML = '';
                            }
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                        errorResponse(xhr.status, errorMessage);
                    }

                });
            });
        }
    );
}

function likerList(like) {
    like.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var post_id = this.dataset.post;
                var url = webroot() + "likes/likerList";
                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        post_id: post_id,
                    },
                    beforeSend: function (xhr) { // Handle csrf errors
                        xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
                    },
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            document.querySelector('#editTitle').innerHTML = 'Recent Likers';
                            document.querySelector('#editForm').innerHTML = data;
                            $('#editModal').modal('show');
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                        errorResponse(xhr.status, errorMessage);
                    }
                });
            });
        }
    );
}
// For follow a person
function follow(foll) {
    foll.addEventListener('click', function (e) {
        e.preventDefault();
        var user_id = this.dataset.user;
        var el = this;
        var url = webroot() + 'followers/follow';
        var root = webroot() + '';
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                user_id: user_id
            },
            cache: false,
            beforeSend: function (xhr) { // Handle csrf errors
                xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
            },
            success: function (data) {

                if (data == null || data == '') {
                    location.reload();
                } else {
                    var followVar = data;
                    if (followVar.deleted) {
                        el.innerHTML = 'Follow';
                    } else {
                        el.innerHTML = 'Unfollow';
                    }

                    var follower_following_count = document.querySelector('#follower_following_count');
                    var followStr = '';
                    if (followVar['follower_count'] > 1) {
                        followStr += followVar['follower_count'] + ' Followers ';
                    } else {
                        followStr += followVar['follower_count'] + ' Follower ';
                    }

                    if (followVar['following_count'] > 1) {
                        followStr += followVar['following_count'] + ' Followings';
                    } else {
                        followStr += followVar['following_count'] + ' Following';
                    }
                    follower_following_count.innerHTML = followStr;
                    fillFollowers(root, followVar['followerList']);
                }
            },
            error: function (xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
                errorResponse(xhr.status, errorMessage);
            }

        });
    });
}

//to update the follower list as user followe certain person
function fillFollowers(root, followers) {
    var followDiv = '';
    var count = 0;
    if (followers.length < 1) {
        followDiv += '<li class="list-group-item text-center"> No Followers...</li>'

    } else {
        console.log('Pumasok dito');
        for (count = 0; count < followers.length; count++) {
            var image = '';
            if (followers[count]['user']['image'] === null) {
                image = root + 'img/user.jpg';
            } else {
                if (root.length > 1) {
                    image = root + '/img/user/' + followers[count]['user']['image'];
                } else {
                    image = '/img/user/' + followers[count]['user']['image'];
                }

            }
            followDiv +=
                `<li class="list-group-item">
                    <div class="div_pic_name">
                        <div class="cropper-profile">
                            <img alt="You" class="pic" src="${image}"/> 
                        </div>
                        &nbsp; 
                        <a href="${root}users/userPage/${followers[count]['user']['id']}">
                            ${followers[count]['user']['username']}
                        </a>
                    </div>
            </li>`;
        }
    }
    document.querySelector('#followers_ul').innerHTML = followDiv;
}

//For some delay in loading data

function lazzyLoader() {
    let output = `<div class="text-center" style="margin-top: 10px">
                    <img src="${webroot()}img/loader.gif" width="100"/>
                </div>`;
    $('#load_data_message').html(output);
}

//For Loading data for the page through scroll pagination
//data and url varies in every page

function loadData(data, url) {
    var page = data.page;
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        cache: false,
        beforeSend: function (xhr) { // Handle csrf errors
            xhr.setRequestHeader('X-CSRF-Token', $('[name="_csrfToken"]').val());
        },
        success: function (data) {
            if (data.length < 80 || data == '') {
                $('#load_data_message').html('<a href="#"><p>No More Result Found...</p></a>');
                loadingAction = 'active';
            } else {
                $('#load_data').append(data);
                $('#load_data_message').html("");
                loadingAction = 'inactive';
            }

            //To determine of for comment
            var parent = document.querySelector('#page' + page);
            if (parent != null) {
                var editCommentLink = parent.querySelectorAll(".edit_comment_link");
                if (editCommentLink != null) {
                    editComment(editCommentLink);
                }
            }
            //To determine if for Post
            var parentForPost = document.querySelector('#post' + page);
            if (parentForPost != null) {
                var retweetLink = document.querySelector('#post' + page).querySelectorAll(".retweet_post_link");
                retweetPost(retweetLink);
                var editPostLink = document.querySelector('#post' + page).querySelectorAll(".edit_post_link");
                editPost(editPostLink);
                var editCommentLink = document.querySelector('#post' + page).querySelectorAll(".edit_comment_link");
                editComment(editCommentLink);
                var coll = document.querySelector('#post' + page).querySelectorAll(".comment-collapsible");
                comment(coll);
                var commentForm = document.querySelector('#post' + page).querySelectorAll(".save-comment");
                submitPostForm(commentForm);
                var data = document.querySelector('#post' + page).querySelectorAll(".like-post");
                like(data);
                var likers = document.querySelector('#post' + page).querySelectorAll(".number-of-likes");
                likerList(likers);
            }

            //To determine of for comment
            var parent = document.querySelector('#search' + page);
            if (parent != null) {
                var likers = document.querySelector('#search' + page).querySelectorAll(".number-of-likes");
                likerList(likers);
            }
        },
        error: function (xhr, status, error) {
            var errorMessage = xhr.status + ': ' + xhr.statusText
            console.log('Error - ' + errorMessage);
            errorResponse(xhr.status, errorMessage);
        }
    });
}


//For Edit Post Modal
function editPost(ePost) {
    ePost.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var url = this.href;
                if (url.search("/posts/edit/") == -1) {
                    window.location.reload();
                }
                $.ajax({
                    url: url,
                    method: "GET",
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            document.querySelector('#editTitle').innerHTML = 'Edit Post';
                            document.querySelector('#editForm').innerHTML = data;
                            var el = document.querySelector('#editForm').querySelector("#input-post");
                            var parentEl = document.querySelector('#editForm');
                            showMaxChar(parentEl, el);
                            var editForm = document.querySelector('#editForm').querySelectorAll('.edit-post-form');
                            submitForm(editForm);
                            $('#editModal').modal('show');
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                        errorResponse(xhr.status, errorMessage);
                    }

                });
            });
        }
    );
}

//For Edit Comment Modal
function editComment(eComment) {
    eComment.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var url = this.href;
                if (url.search("/comments/edit/") == -1) {
                    window.location.reload();
                }
                $.ajax({
                    url: url,
                    method: "GET",
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            document.querySelector('#editTitle').innerHTML = 'Edit Comment';
                            document.querySelector('#editForm').innerHTML = data;
                            var el = document.querySelector('#editForm').querySelector("#input-post");
                            var parentEl = document.querySelector('#editForm');
                            showMaxChar(parentEl, el);
                            var editForm = document.querySelector('#editForm').querySelectorAll('.edit-comment-form');
                            submitForm(editForm);
                            $('#editModal').modal('show');
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                        errorResponse(xhr.status, errorMessage);
                    }
                });
            });
        }
    );
}

//For Retweet Post Modal

function retweetPost(rPost) {
    rPost.forEach(
        _element => {
            _element.addEventListener("click", function (e) {
                e.preventDefault();
                var url = this.href;
                if (url.search("/posts/retweet/") == -1) {
                    window.location.reload();
                }
                $.ajax({
                    url: url,
                    method: "GET",
                    cache: false,
                    success: function (data) {
                        if (data == null || data == '') {
                            location.reload();
                        } else {
                            document.querySelector('#editTitle').innerHTML = 'Retweet Post';
                            document.querySelector('#editForm').innerHTML = data;
                            var el = document.querySelector('#editForm').querySelector("#input-post");
                            var parentEl = document.querySelector('#editForm');
                            var retweetForm = document.querySelector('#editForm').querySelectorAll('.retweet-form');
                            submitForm(retweetForm);
                            showMaxChar(parentEl, el);
                            $('#editModal').modal('show');
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                        errorResponse(xhr.status, errorMessage);
                    }

                });
            });
        }
    );
}

//For Showing the Max Character For Every Input Inside a ajax request

function showMaxChar(parent, input_post) {
    var maxCharLimit = 140;

    if (input_post != null) {
        parent.querySelector("#input-post-count").value = input_post.value.length + '/' + maxCharLimit;
        input_post.addEventListener("keyup", function (e) {
            var lengthCount = this.value.length;
            if (lengthCount == maxCharLimit) {
                e.preventDefault();
            }
            parent.querySelector("#input-post-count").value = lengthCount + '/' + maxCharLimit;
        });
    }
}

function submitPostForm(postForm) {
    postForm.forEach(
        _element => {
            _element.addEventListener('submit', function (e) {
                e.preventDefault();
                var url = $(this).prop('action');
                var form = $(this);
                let btn = form.find('[type="submit"]');
                btn.prop('disabled', true);
                var formData = new FormData(form[0]);
                if (!fileSizeValidation(null)) {
                    btn.prop('disabled', false);
                    return false;
                }
                $.ajax({
                    url: url,
                    data: formData,
                    type: 'POST',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.hasOwnProperty('error')) {
                            btn.prop('disabled', false);
                            var data = response.errorList;
                            var msg;
                            $('.response-message').html('');
                            for (const key in data) {
                                if (data.hasOwnProperty(key)) {
                                    const errorMessage = Object.values(data[key]);
                                    msg = errorMessage + '<br/>';
                                }
                            }

                            document.querySelector('#editTitle').innerHTML = '<span class="text-danger">Error!</span>';
                            document.querySelector('#editForm').innerHTML = msg;
                            $('#editModal').modal('show');

                        } else {
                            //console.log(response);
                            window.location.reload();
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                        //window.location.reload();
                        errorResponse(xhr.status, errorMessage);
                    }
                });
            });
        }
    );
}

function submitForm(theForm) {
    theForm.forEach(
        _element => {
            _element.addEventListener('submit', function (e) {
                e.preventDefault();
                url = $(this).prop('action');
                form = $(this);
                let btn = form.find('[type="submit"]');
                btn.prop('disabled', true);
                var formData = new FormData(form[0]);
                if (!fileSizeValidation(form)) {
                    btn.prop('disabled', false);
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        let input = form.find('.form__input');
                        for (const key in input) {
                            input.siblings('.form__input-error').html('');
                        }
                        if (response.hasOwnProperty('error')) {
                            btn.prop('disabled', false);
                            var data = response.errorList;

                            for (const key in data) {
                                if (data.hasOwnProperty(key)) {
                                    const errorMessage = Object.values(data[key]);
                                    form.find('[name="' + key + '"]').siblings('.form__input-error').html(errorMessage[0]);
                                }
                            }
                        } else {
                            window.location.reload();
                        }
                    },
                    error: function (xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        console.log('Error - ' + errorMessage);
                        //window.location.reload();
                        errorResponse(xhr.status, errorMessage);
                    }
                });
            });
        }
    );
}

//Validate File Size
function fileSizeValidation(option) {
    let fi;
    let key;

    let f1 = document.getElementById('post-image');
    let f2 = document.getElementById('image');
    let f3 = document.getElementById('post-image-edit');

    if (f3 != null) {
        fi = f3;
        key = 'post_image'
    } else if (f2 != null) {
        fi = f2;
        key = 'image';
    } else {
        fi = f1;
    }

    if (fi == null) {
        return true;
    }
    // Check if any file is selected. 
    if (fi.files.length > 0) {
        for (let i = 0; i <= fi.files.length - 1; i++) {

            let fsize = fi.files.item(i).size;
            let file = Math.round((fsize / 1024));
            // The size of the file. 
            if (file > 2048) {
                if (option == null) {
                    document.querySelector('#editTitle').innerHTML = '<span class="text-danger">Error!</span>';
                    document.querySelector('#editForm').innerHTML = 'This file is too large, 2MB is the max size of the file';
                    $('#editModal').modal('show');
                } else {
                    let input = option.find('.form__input');
                    for (const key in input) {
                        input.siblings('.form__input-error').html('');
                    }
                    option.find('[name="' + key + '"]').siblings('.form__input-error').html('This file is too large, 2MB is the max size of the file');
                }
                return false;
            }
        }
    }
    return true;
}

function errorResponse(status, errorMessage) {
    switch (status) {
        case 400:
            window.location.reload();
            break;
        case 403:
            alert('Error occured upon uploading a request');
            break;
        case 404:
            alert('Route did not found');
            break;
        default:
            alert(errorMessage + ', Please contact server administrator.');
            break;
    }
    window.location.reload();
}