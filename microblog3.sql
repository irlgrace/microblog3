-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2020 at 10:14 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microblog3`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL,
  `comment` varchar(140) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `user_id`, `post_id`, `created`, `modified`, `deleted_date`, `deleted`) VALUES
(1, 'comments ', 2, 2, '2020-03-03 01:30:27', '2020-03-03 01:30:27', NULL, 0),
(2, 'comment', 3, 4, '2020-03-03 01:37:04', '2020-03-03 01:37:04', NULL, 0),
(3, 'doggo', 3, 2, '2020-03-03 01:39:23', '2020-03-03 01:39:23', NULL, 0),
(4, 'german shepherd', 4, 2, '2020-03-03 01:46:24', '2020-03-03 01:46:24', NULL, 0),
(5, 'nylle', 5, 4, '2020-03-03 01:52:09', '2020-03-03 01:52:09', NULL, 0),
(6, '<script>alert(\'Hi!\');</script>', 10, 14, '2020-03-03 05:47:43', '2020-03-03 05:47:43', NULL, 0),
(7, 'wow', 1, 14, '2020-03-03 06:43:34', '2020-03-03 06:43:34', NULL, 0),
(8, '<script>alert(\'Yown\');</script>', 1, 11, '2020-03-03 06:45:40', '2020-03-03 06:45:40', NULL, 0),
(9, 'comment sample', 2, 2, '2020-03-03 09:01:48', '2020-03-03 09:01:48', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `following_user_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `following_user_id`, `created`, `modified`, `deleted`) VALUES
(1, 2, 1, '2020-03-03 01:30:04', '2020-03-03 05:07:02', 0),
(2, 3, 1, '2020-03-03 01:39:03', '2020-03-03 01:39:03', 0),
(3, 3, 2, '2020-03-03 01:39:07', '2020-03-03 01:39:07', 0),
(4, 4, 3, '2020-03-03 01:44:47', '2020-03-03 01:44:47', 0),
(5, 4, 2, '2020-03-03 01:44:55', '2020-03-03 01:44:59', 0),
(6, 4, 1, '2020-03-03 01:45:05', '2020-03-03 01:45:05', 0),
(7, 5, 1, '2020-03-03 01:50:56', '2020-03-03 01:50:56', 0),
(8, 5, 4, '2020-03-03 01:51:10', '2020-03-03 01:51:10', 0),
(9, 5, 2, '2020-03-03 01:51:27', '2020-03-03 01:51:27', 0),
(10, 5, 3, '2020-03-03 01:51:57', '2020-03-03 01:51:57', 0),
(11, 6, 1, '2020-03-03 01:54:44', '2020-03-03 01:54:44', 0),
(12, 7, 1, '2020-03-03 02:29:47', '2020-03-03 02:29:47', 0),
(13, 8, 1, '2020-03-03 05:08:46', '2020-03-03 05:12:02', 0),
(14, 1, 4, '2020-03-03 05:20:29', '2020-03-03 06:39:08', 0),
(15, 1, 5, '2020-03-03 05:25:14', '2020-03-03 05:25:14', 0),
(16, 1, 2, '2020-03-03 05:25:55', '2020-03-03 05:25:55', 0),
(17, 1, 7, '2020-03-03 05:26:14', '2020-03-03 05:26:14', 0),
(18, 1, 8, '2020-03-03 05:27:03', '2020-03-03 05:27:03', 0),
(19, 9, 1, '2020-03-03 05:43:42', '2020-03-03 05:43:42', 0),
(20, 10, 1, '2020-03-03 05:45:10', '2020-03-03 05:45:10', 0),
(21, 11, 1, '2020-03-03 06:28:08', '2020-03-03 06:28:08', 0),
(22, 12, 1, '2020-03-03 06:29:01', '2020-03-03 06:29:01', 0),
(23, 1, 3, '2020-03-03 06:39:01', '2020-03-03 06:39:01', 0),
(24, 1, 6, '2020-03-03 06:39:21', '2020-03-03 06:39:23', 0),
(25, 1, 12, '2020-03-03 06:39:46', '2020-03-03 06:39:49', 0),
(26, 1, 11, '2020-03-03 06:39:58', '2020-03-03 06:39:58', 0),
(27, 1, 10, '2020-03-03 06:43:24', '2020-03-03 06:43:24', 0),
(28, 1, 9, '2020-03-03 06:50:44', '2020-03-03 06:50:44', 0),
(29, 13, 1, '2020-03-03 07:58:29', '2020-03-03 08:05:02', 0),
(30, 14, 1, '2020-03-03 09:10:35', '2020-03-03 09:10:35', 0),
(31, 14, 2, '2020-03-03 09:11:31', '2020-03-03 09:11:31', 0);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `user_id`, `created`, `modified`, `deleted`) VALUES
(1, 2, 2, '2020-03-03 01:30:07', '2020-03-03 05:13:18', 0),
(2, 2, 3, '2020-03-03 01:39:17', '2020-03-03 01:39:17', 0),
(3, 4, 3, '2020-03-03 01:40:39', '2020-03-03 01:40:47', 0),
(4, 2, 4, '2020-03-03 01:45:36', '2020-03-03 01:45:36', 0),
(5, 4, 4, '2020-03-03 01:45:45', '2020-03-03 01:45:45', 0),
(6, 6, 5, '2020-03-03 01:50:49', '2020-03-03 01:50:49', 0),
(7, 2, 5, '2020-03-03 01:51:04', '2020-03-03 01:51:04', 0),
(8, 5, 5, '2020-03-03 01:51:11', '2020-03-03 01:51:11', 0),
(9, 3, 5, '2020-03-03 01:51:30', '2020-03-03 01:51:30', 0),
(10, 4, 5, '2020-03-03 01:51:59', '2020-03-03 01:51:59', 0),
(11, 2, 6, '2020-03-03 01:54:49', '2020-03-03 01:54:49', 0),
(12, 2, 7, '2020-03-03 02:29:50', '2020-03-03 02:52:21', 0),
(13, 1, 7, '2020-03-03 02:39:00', '2020-03-03 02:39:00', 0),
(14, 9, 7, '2020-03-03 03:59:49', '2020-03-03 04:00:09', 0),
(15, 9, 2, '2020-03-03 05:02:18', '2020-03-03 05:02:18', 0),
(16, 2, 8, '2020-03-03 05:09:03', '2020-03-03 05:11:01', 0),
(17, 2, 1, '2020-03-03 05:12:21', '2020-03-03 05:12:21', 0),
(18, 5, 1, '2020-03-03 05:22:58', '2020-03-03 05:22:58', 0),
(19, 7, 1, '2020-03-03 05:25:15', '2020-03-03 05:25:15', 0),
(20, 9, 1, '2020-03-03 05:26:19', '2020-03-03 05:26:23', 0),
(21, 12, 5, '2020-03-03 05:31:24', '2020-03-03 05:31:24', 0),
(22, 2, 9, '2020-03-03 05:43:56', '2020-03-03 05:43:56', 0),
(23, 14, 1, '2020-03-03 06:43:27', '2020-03-03 06:43:27', 0),
(24, 11, 1, '2020-03-03 06:46:00', '2020-03-03 06:46:00', 0),
(25, 16, 1, '2020-03-03 06:48:19', '2020-03-03 06:48:19', 0),
(26, 2, 13, '2020-03-03 08:16:00', '2020-03-03 08:16:00', 0),
(27, 18, 13, '2020-03-03 08:49:50', '2020-03-03 08:49:50', 0),
(28, 17, 13, '2020-03-03 08:49:54', '2020-03-03 08:49:54', 0),
(29, 5, 2, '2020-03-03 08:51:24', '2020-03-03 08:51:24', 0),
(30, 3, 2, '2020-03-03 09:01:30', '2020-03-03 09:01:30', 0),
(31, 19, 2, '2020-03-03 09:03:37', '2020-03-03 09:03:37', 0),
(32, 2, 14, '2020-03-03 09:10:42', '2020-03-03 09:10:42', 0),
(33, 16, 14, '2020-03-03 09:12:34', '2020-03-03 09:12:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) NOT NULL,
  `post` varchar(140) NOT NULL,
  `post_image` varchar(225) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `retweeted_post_id` bigint(20) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `comment_count` int(11) NOT NULL DEFAULT 0,
  `like_count` int(11) NOT NULL DEFAULT 0,
  `retweet_count` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post`, `post_image`, `user_id`, `retweeted_post_id`, `created`, `modified`, `deleted_date`, `deleted`, `comment_count`, `like_count`, `retweet_count`) VALUES
(1, 'First post', NULL, 1, NULL, '2020-03-03 01:27:38', '2020-03-03 01:27:38', NULL, 0, 0, 1, 0),
(2, 'Morning doggooo', 'e2cc3bcede050e0fdb0c2860c103e66d.jpg', 1, NULL, '2020-03-03 01:28:11', '2020-03-03 09:12:13', NULL, 0, 4, 11, 2),
(3, 'doggo', NULL, 2, 2, '2020-03-03 01:30:47', '2020-03-03 01:51:42', NULL, 0, 0, 2, 1),
(4, 'post', NULL, 3, NULL, '2020-03-03 01:36:52', '2020-03-03 05:30:25', NULL, 0, 2, 3, 1),
(5, 'cheers ', NULL, 4, NULL, '2020-03-03 01:48:03', '2020-03-03 06:42:57', NULL, 0, 0, 3, 1),
(6, 'try', NULL, 5, NULL, '2020-03-03 01:50:43', '2020-03-03 01:50:43', NULL, 0, 0, 1, 0),
(7, 'dog <3', NULL, 5, 2, '2020-03-03 01:51:42', '2020-03-03 05:25:25', NULL, 0, 0, 1, 1),
(8, 'sample ', NULL, 7, NULL, '2020-03-03 03:46:13', '2020-03-03 03:49:09', '2020-03-03 03:49:09', 1, 0, 0, 0),
(9, 'post again', NULL, 7, NULL, '2020-03-03 03:49:22', '2020-03-03 03:49:22', NULL, 0, 0, 3, 0),
(10, 'cutieee', NULL, 1, 2, '2020-03-03 05:25:25', '2020-03-03 05:46:50', NULL, 0, 0, 0, 2),
(11, 'brownniiieeee', NULL, 5, 2, '2020-03-03 05:30:02', '2020-03-03 05:30:02', NULL, 0, 1, 1, 0),
(12, 'retweet post', NULL, 5, 4, '2020-03-03 05:30:25', '2020-03-03 06:42:40', NULL, 0, 0, 1, 1),
(13, 'sheppphhheeerrrdddddddd', NULL, 10, 2, '2020-03-03 05:46:50', '2020-03-03 05:46:50', NULL, 0, 0, 0, 0),
(14, '<script>alert(\'Hi!\');</script>', NULL, 10, NULL, '2020-03-03 05:47:22', '2020-03-03 05:47:22', NULL, 0, 2, 1, 0),
(15, 'retweet', NULL, 1, 4, '2020-03-03 06:42:40', '2020-03-03 06:42:40', NULL, 0, 0, 0, 0),
(16, '...', NULL, 1, 5, '2020-03-03 06:42:57', '2020-03-03 06:42:57', NULL, 0, 0, 2, 0),
(17, 'post', NULL, 13, NULL, '2020-03-03 08:26:43', '2020-03-03 08:26:43', NULL, 0, 0, 1, 0),
(18, 'again lala edit yowwwn', NULL, 13, NULL, '2020-03-03 08:33:00', '2020-03-03 08:36:28', NULL, 0, 0, 1, 0),
(19, 'your grace <3', '2f4f50048dcb021ed33b6f924dc6405f.jpg', 2, NULL, '2020-03-03 09:03:32', '2020-03-03 09:11:42', NULL, 0, 0, 1, 1),
(20, 'samoyyyeeeeeeeeeeeeeeeeeeeeeeeeed ', NULL, 14, 19, '2020-03-03 09:11:42', '2020-03-03 09:11:42', NULL, 0, 0, 0, 0),
(21, 'cutie', NULL, 14, 2, '2020-03-03 09:12:13', '2020-03-03 09:12:13', NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(500) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT 0,
  `activation_code` varchar(8) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_date` datetime DEFAULT NULL,
  `follower_count` int(11) NOT NULL DEFAULT 0,
  `following_count` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `image`, `activated`, `activation_code`, `created`, `modified`, `deleted`, `deleted_date`, `follower_count`, `following_count`) VALUES
(1, 'irlgrace', 'irlgrace@getnada.com', '$2y$10$Cx4DZAGEVmAIHEKeuqwy9ecNTgzBa0Cz.je/c0tS.nygsiduSy3RW', NULL, 1, '04d694c8', '2020-03-03 01:22:42', '2020-03-03 01:23:09', 0, NULL, 13, 11),
(2, 'grace', 'grace@getnada.com', '$2y$10$nwXv0BmIwo6DRJC5Cs/eFOji7FuxRzUJY4sP/MUADGb/hX.pt.7v2', NULL, 1, '6365945', '2020-03-03 01:29:19', '2020-03-03 01:29:45', 0, NULL, 5, 1),
(3, 'grace.irlandez', 'grace.irlandez@getnada.com', '$2y$10$kmP.pkzGV3l7F0rCW11K4uWsi.uZ9TBsoqxmqXVwgvwDRcl8FRWHa', '7a81da405c32dfc0a262c3cc59769407.png', 1, 'b6b4677f', '2020-03-03 01:35:14', '2020-03-03 01:39:41', 0, NULL, 3, 2),
(4, 'nylle.brian', 'nylle.brian@getnada.com', '$2y$10$bP2.1gCTJno4FpV6HWJA..MJgsW9AogAPZTn/dWcbl8bWCuRF.AG.', '5a9d8b8fd3757b437b408a85f2d161f9.jpg', 1, 'aa43d5af', '2020-03-03 01:43:43', '2020-03-03 01:46:55', 0, NULL, 2, 3),
(5, 'nylle', 'nylle@getnada.com', '$2y$10$T1emx2fsLsDLSynXmcUxHecXSjl/DOM1NTXIVm7blpT25OEa1cVRq', NULL, 1, '42a88c3c', '2020-03-03 01:49:22', '2020-03-03 01:49:52', 0, NULL, 1, 4),
(6, 'nylle17', 'nylle17@getnada.com', '$2y$10$g7H0WMt4gt0CvUeMi5R0keC9.DEtNrMCdAw79/1JU7u59N4VgLriS', NULL, 1, '2223467b', '2020-03-03 01:53:58', '2020-03-03 01:54:34', 0, NULL, 1, 1),
(7, 'nyllie', 'nyllie@getnada.com', '$2y$10$XGrU4975Z7.YmbB8vmknJeS7bXJev6UCD.pd5QhUrdsOn27K2nqHa', NULL, 1, '6d686390', '2020-03-03 02:29:17', '2020-03-03 02:29:31', 0, NULL, 1, 1),
(8, 'grace19', 'grace19@getnada.com', '$2y$10$cHc.kh9B/YkMIxAJx7/7kulQY8c6jBDer6vt4GVn5n0uu00SeWRfq', NULL, 1, 'a1d245d6', '2020-03-03 05:07:41', '2020-03-03 05:08:31', 0, NULL, 1, 1),
(9, 'bravo', 'bravo@getnada.com', '$2y$10$71VBp2bNJd1Bi5k7ocWWLO/vAkmewJZdk/8BV6WWqxTJcRODpo/Na', NULL, 1, 'ec6eb3ee', '2020-03-03 05:43:01', '2020-03-03 05:43:24', 0, NULL, 1, 1),
(10, 'alpha', 'alphaness@getnada.com', '$2y$10$aN62V1uRNOXmkF.XRsLKfeGEVNR4PXAl5u48b1VAKwGjSHEa481aW', NULL, 1, 'e8b81ee9', '2020-03-03 05:44:42', '2020-03-03 05:45:02', 0, NULL, 1, 1),
(11, 'alpha.bravo', 'alpha.bravo@getnada.com', '$2y$10$li/l8.ojekxkGqbi2pkmo.B/vV8uGsuzQElWgjtyftcQGHzTvAvXS', NULL, 1, 'f69339fc', '2020-03-03 05:50:21', '2020-03-03 06:13:09', 0, NULL, 1, 1),
(12, 'al', 'al@getnada.com', '$2y$10$iKr2xRYNt6OD744rEqk7MuCIgknr8PAjdK0NXm9kCyZYeQrmtRG.6', NULL, 1, '709cdc7b', '2020-03-03 06:28:41', '2020-03-03 06:28:54', 0, NULL, 1, 1),
(13, 'krissy', 'krissy@getnada.com', '$2y$10$BBYFbZkhCLFW1fpJGrpwMevVkPtzfuZAyCAdLVKb/hQKQHumBAZIa', NULL, 1, '06b9bfc1', '2020-03-03 07:24:37', '2020-03-03 07:24:58', 0, NULL, 0, 1),
(14, 'another', 'another@getnada.com', '$2y$10$dLFxLhleOJpiRdp2.0.X/uZ0JmL8WkrFi99JXsxjtsawpupHitFNW', NULL, 1, 'd93d28', '2020-03-03 09:09:00', '2020-03-03 09:10:16', 0, NULL, 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
