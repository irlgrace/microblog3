<?php if (!isset($error)) { ?>
    <?= $this->Form->create(
        $retweetPost,
        [
            'url' => ['controller' => 'Posts', 'action' => 'retweetPost'],
            'class' => 'retweet-form'
        ]
    ) ?>
    <span class="form__input-error text-danger"></span>
    <?= $this->Form->control('post', [
        'templates' => [
            'inputContainer' => '{{content}}'
        ],
        'label' => false,
        'id' => 'input-post',
        'class' => 'form-control form__input',
        'type' => 'textarea',
        'rows' => '2',
        'required' => false,
        'placeholder' => 'Say something...'
    ])
    ?>
    <div class='div_post'>
        <!--start-->
        <div class='div_pic_name'>
            <?php
            echo '<div class="cropper-profile">'; 
            echo $this->Html->image(
                empty($post->user->image) ? 'user.jpg' : '/img/user/' . h($post->user->image),
                [
                    'class' => 'pic'
                ]
            );
            echo '</div>';
            echo '&nbsp;';
            echo '<h6>' . $this->Html->link(
                h($post->user->username),
                ['controller' => 'users', 'action' => 'userPage', h($post->user_id)],
            ) . '</h6>';
            ?>
        </div>
        <p><?= h($post->post) ?></p>
        <?php
        if ($post->post_image != null) {
            echo '<div class="card bg-secondary">';
            echo $this->Html->image(
                '/img/post/' . h($post->post_image),
                [
                    'style' => 'width: 200px;',
                    'class' => 'center'
                ]
            );
            echo '</div>';
        }
        ?>
    </div>
    <!--end-->
    <br />
    <div class='float-right'>
        <div class='input-group' id='button_post_div'>
            <input type='text' id='input-post-count' disabled size='4' />
            &nbsp;
            <?= $this->Form->control('retweeted_post_id', [
                'label' => false,
                'type' => 'hidden',
                'value' => empty($post->retweeted_post_id) ? h($post->id) : h($post->retweeted_post_id)
            ])
            ?>
            <?= $this->Form->control('ref_post_id', [
                'label' => false,
                'type' => 'hidden',
                'value' => h($post->id)
            ])
            ?>
            <?= $this->Form->button(__('Post'), [
                'class' => 'btn-sm btn-success'
            ])
            ?>
            <?= $this->Form->end(); ?>
        </div>
    </div>
<?php } ?>