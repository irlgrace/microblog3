<?php
if (!isset($error)) {
?>
    <?= $this->Form->create($post, [
        'type' => 'file',
        'url' => ['controller' => 'Posts', 'action' => 'editPost'],
        'class' => 'edit-post-form'
    ])
    ?>
    <?= $this->Form->control('post', [
        'label' => false,
        'id' => 'input-post',
        'class' => 'form-control form__input',
        'type' => 'textarea',
        'required' => false,
        'templates' => [
            'inputContainer' => '{{content}}'
        ],
    ])
    ?>
    <span class="form__input-error text-danger"></span>

    <div class='float-left'>
        <div id='button_post_div'>
            <?php
            if ($post->retweeted_post_id == null) {
                echo $this->Form->control('post_image', [
                    'label' => false,
                    'type' => 'file',
                    'required' => false,
                    'templates' => [
                        'inputContainer' => '{{content}}'
                    ],
                    'id' => 'post-image-edit',
                    'class' => 'form__input'
                ]);
                echo '<br/><span class="form__input-error text-danger"></span>';
            }
            if ($post->post_image != null) {
                echo $this->Form->control('remove_image', [
                    'label' => 'Remove Image',
                    'type' => 'checkbox',
                ]);
            }
            ?>
        </div>
    </div>
    <div class="float-right">
        <div class="input-group" id="button_post_div">
            <input type="text" id="input-post-count" disabled size="4" />
            &nbsp;
            <?=$this->Form->button(
                    _('Save Post'),
                    [
                        'class' => 'btn-sm btn-success',
                    ]
                )
            ?>
            <?= $this->Form->end()?>
        </div>
    </div>
<?php } ?>