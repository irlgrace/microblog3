<div id='search<?= h($page) ?>'>
    <?php foreach ($posts as $post) : ?>
        <div class='div_post card'>
            <!--start-->
            <div class='div-post-upper'>
                <div class='div_pic_name'>
                    <div class='cropper-profile'>
                    <?php echo $this->Html->image(
                        empty($post->user->image) ? 'user.jpg' : '/img/user/' . h($post->user->image),
                        [
                            'class' => 'pic'
                        ]
                    );
                    ?>
                    </div>
                    &nbsp;
                    <h6>
                        <?= $this->Html->link(
                            h($post->user->username),
                            ['controller' => 'Users', 'action' => 'userPage', h($post->user->id)],
                        )
                        ?>
                    </h6>
                </div>
                <p><?= h($post->post) ?></p>
                <?php
                if ($post->post_image != null) {
                    echo '<div class="card bg-secondary">';
                    echo $this->Html->image(
                        '/img/post/' . h($post->post_image),
                        [
                            'class' => 'center post_img',
                            'alt' => h($post->post)
                        ]
                    );
                    echo '</div>';
                }
                ?>
                <?php if ($post->retweeted_post_id != null) { ?>
                    <a href="<?= $this->request->getAttribute('webroot') . 'posts/view/' . h($post->retweeted_post_id) ?>" class='post-view-link'>
                        <div class='div_post card'>
                            <?php if ($post->retweeted_post->deleted) {
                                echo '<p class="text-danger">The post has been removed.</p>';
                            } else {
                                echo '<div class="div_pic_name">';
                                echo '<div class="cropper-profile">';
                                echo $this->Html->image(
                                    empty($post->retweeted_post->user->image)
                                        ? 'user.jpg'
                                        : '/img/user/' . h($post->retweeted_post->user->image),
                                    [
                                        'class' => 'pic'
                                    ]
                                );
                                echo '</div>';
                                echo '&nbsp;';
                                echo '<h6>' . h($post->retweeted_post->user->username) . '</h6>';
                                echo '</div>';
                                echo '<p>' . h($post->retweeted_post->post) . '</p>';

                                if ($post->retweeted_post->post_image != null) {
                                    echo '<div class="card bg-secondary">';
                                    echo $this->Html->image(
                                        '/img/post/' . h($post->retweeted_post->post_image),
                                        [
                                            'class' => 'center post_img',
                                            'alt' => h($post->retweeted_post->post)
                                        ]
                                    );
                                    echo '</div>';
                                }
                            } ?>
                        </div>
                    </a>
                <?php } ?>
            </div>
            <div class='d-flex justify-content-between'>
                <div class='float-left'>
                    <?php
                    if ($post->like_count > 0) {
                        echo "<a href='#' class='number-of-likes' data-post='$post->id'>";
                        echo $post->like_count > 1 ?
                            h($post->like_count) . ' Likes' :
                            h($post->like_count) . ' Like';

                        echo "</a>";
                    } 
                    if ($post->comment_count > 0) {
                        echo '&nbsp';
                        echo $this->Html->link(
                            $post->comment_count > 1 ?
                                h($post->comment_count) . ' Comments' :
                                h($post->comment_count) . ' Comment',
                            ['controller' => 'Comments', 'action' => 'seeMore', h($post->id)],
                        );
                    }
                    if ($post->retweet_count > 0) {
                        echo '&nbsp';
                        echo '<span class="text-primary">';
                        echo $post->retweet_count > 1 ? h($post->retweet_count) . ' Retweets' :
                            h($post->retweet_count) . ' Retweet';
                        echo '</span>';
                    }
                    ?>
                </div>
                <div class='float-right'>
                    <div class='dropdown'>
                        <button class='dropbtn'>...</button>
                        <div class='dropdown-content'>
                            <?= $this->Html->link(
                                'View',
                                ['controller' => 'posts', 'action' => 'view', h($post->id)],
                            )
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>