<div id='content' class='row'>
    <div class='col-md-5 col-sm-5 col-lg-5'>
        <div class='card bg-secondary text-center' id='me_card' style='position: fixed; width: 25%; height: 75%;'>
            <div class='card-body'>
                <div class="image-cropper center" style="margin-top:30%">
                <?= $this->Html->image(
                    empty($me['image']) ? 'user.jpg' : '/img/user/' . h($me['image']),
                    [
                        'alt' => 'You',
                        'class' => 'profile-pic',
                    ]
                ) ?>
                </div>
                <br />
                <div class='line'></div>
                <h5 class='card-title'>
                    <?php echo $this->Html->link(
                        h($me['username']),
                        [
                            'controller' => 'users',
                            'action' => 'userPage', h($me['id'])
                        ],
                        [
                            'class' => 'white',
                        ]
                    );
                    ?>
                </h5>
                <p class='white'><?= h($me['email']) ?></p>
            </div>
        </div>
    </div>
    <div class='col-md-7 col-sm-7 col-lg-7'>
        <div id='div_post_input' class='card'>
            <?= $this->Form->create($newPost, [
                'url' => ['controller' => 'posts', 'action' => 'create'],
                'type' => 'file',
                'class' => 'save-post'
            ])
            ?>

            <div class='input-group'>
                <?= $this->Form->control('post', [
                    'templates' => [
                        'inputContainer' => '{{content}}'
                    ],
                    'label' => false,
                    'id' => 'input-post',
                    'class' => 'form-control',
                    'type' => 'textarea',
                    'rows' => '2',
                    'required' => false,
                    'placeholder' => 'Say something...'
                ])
                ?>
            </div>
            <div class='float-left'>
                <div id='button_post_div'>
                    <?= $this->Form->control('post_image', [
                        'label' => false,
                        'type' => 'file'
                    ])
                    ?>
                </div>
            </div>
            <div class='float-right'>
                <div class='input-group' id='button_post_div'>
                    <input type='text' id='input-post-count' disabled size='4' />
                    &nbsp;
                    <?= $this->Form->button(
                        __('Post'),
                        [
                            'label' => 'Post',
                            'class' => 'btn-sm btn-success'
                        ]
                    )
                    ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
        <div id='load_data'></div>
        <div id='load_data_message'></div>
    </div>
</div>
<!--end of content-->
<script>
    initIndex();

    var url = getPostUrl();
    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
        }
    }
</script>