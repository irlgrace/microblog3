<div id='content' class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class='card'>
            <div class='card-body'>
                <div class='row'>
                    <div class='col-md-12 col-lg-12'>
                        <div class='float-left'>
                            <input type='hidden' id='search_word' value='<?=h($search)?>' />
                            <h4>Search Results</h4>
                        </div>
                        <div class='float-right d-flex flex-row'>
                            <div class='user_search_div'>
                                <?php
                                    echo $this->Form->create(false, [
                                            'url' => ['controller' => 'users', 'action' => 'searchUser'],
                                            'type' => 'get'
                                        ],
                                    ); 
                                    echo $this->Form->control('search', [
                                        'type' => 'hidden',
                                        'value' => h($search)
                                    ]);
                                    echo $this->Form->button(__('User'),
                                    [
                                        'class' => 'btn btn-primary w-100',
                                        'templates' => [
                                            'inputContainer' => '{{content}}'
                                        ],
                                    ]);
                                    echo $this->Form->end();
                                ?>
                            </div>
                            &nbsp;
                            <div class='post_search_div'>
                                <?php
                                    echo $this->Form->create(false, [
                                            'url' => ['controller' => 'posts', 'action' => 'searchPost'],
                                            'type' => 'get'
                                        ],
                                    ); 
                                    echo $this->Form->control('search', [
                                        'type' => 'hidden',
                                        'value' => h($search)
                                    ]);
                                    echo $this->Form->button(__('Post'),
                                    [
                                        'class' => 'btn btn-primary w-100 active',
                                        'templates' => [
                                            'inputContainer' => '{{content}}'
                                        ],
                                    ]);
                                    echo $this->Form->end();
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class='row'>
                    <div class='col-md-12 col-lg-12'>
                        <div id='load_data'></div>
                        <div id='load_data_message'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //FOR PAGINATION
    var url = getSearchUrl();
    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
            search: getSearchWord()
        }
    }
</script>