<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title ?></title>
    <?php
    echo $this->Html->css(['bootstrap.min', 'inside_design']);
    echo $this->Html->script(['jquery-3.4.1.min', 'popper.min', 'bootstrap.min', 'inside_js']);
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
</head>

<body>
    <?= $this->element('inside_header') ?>
    <div class='fixed container' style='margin-top: 100px'>
        <div class='row'>
            <div class='col-md-7 col-lg-7 offset-md-5 offset-lg-5'>
                <?= $this->Flash->render() ?>
                <div class='response-message'>

                </div>
            </div>
        </div>
        <?php
        echo $this->fetch('content');
        echo $this->element('edit_modal');
        ?>
    </div>
    <?= $this->Html->script('inside') ?>
</body>

</html>