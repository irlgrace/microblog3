<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php echo $this->fetch('title'); ?></title>
</head>
<body style="font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;">
    <h3>Hi <b><?php echo $name; ?></b>!</h3>
    <br/>
    <p>Welcome to Microblog 3! You just created an account in our site. Please Activate the account.</p>
    <br/>
    <br/>
    Click <a href='<?php echo $verification_link; ?>' target='_blank'>here</a> to activate the account.
    <br/>
    <br/>
    Or go to this link :
    <a href='<?php echo $verification_link; ?>' target='_blank'><?php echo $verification_link; ?></a>
    <?php echo $this->fetch('content'); ?>
    <br/>
    <br/>
    <p>Thank you.</p>
</body>
</html>