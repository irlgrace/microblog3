<!DOCTYPE html>
<html>

<head>
    <title>
        <?= $title ?>
    </title>
    <?= $this->Html->css(['bootstrap.min', 'front_design']) ?>
    <?= $this->Html->script(['jquery-3.3.1.slim.min', 'popper.min', 'bootstrap.min']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>

<body>
    <?= $this->element('outside_header'); ?>
    <div class='container'>
        <br />
        <div id='content' class='row'>
            <div class='col-md-6'>
                <div class='content_div' id='div_image'>
                    <?= $this->Html->image('front_background.jpg', ['alt' => 'Microblog 3']) ?>
                </div>
            </div>
            <div class='col-md-6'>
                <div class='float-right'>
                    <?= $this->fetch('content') ?>
                </div>
            </div>
        </div>
        <br />
    </div>
    <div id='footer'></div>
</body>
</html>