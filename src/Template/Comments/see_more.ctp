<div class='row'>
    <div class='col-md-5 col-lg-5'>
        <!--start-->
        <div class='div_post card'>
            <!--start-->
            <div class='div-post-upper'>
                <div class='div_pic_name'>
                    <div class='cropper-profile'>
                    <?php echo $this->Html->image(
                        empty($post->user->image) ? 'user.jpg' : '/img/user/' . h($post->user->image),
                        [
                            'class' => 'pic'
                        ]
                    );
                    ?>
                    </div>
                    &nbsp;
                    <h6>
                        <?= $this->Html->link(
                            h($post->user->username),
                            ['controller' => 'Users', 'action' => 'userPage', h($post->user->id)],
                        )
                        ?>
                    </h6>
                </div>
                <p><?= h($post->post) ?></p>
                <?php
                if ($post->post_image != null) {
                    echo '<div class="card bg-secondary">';
                    echo $this->Html->image(
                        '/img/post/' . h($post->post_image),
                        [
                            'class' => 'center post_img',
                            'alt' => h($post->post)
                        ]
                    );
                    echo '</div>';
                }
                ?>
                <?php if ($post->retweeted_post_id != null) { ?>
                    <a href="<?= $this->request->getAttribute('webroot') . 'posts/view/' . h($post->retweeted_post_id) ?>" class='post-view-link'>
                        <div class='div_post card'>
                            <?php if ($post->retweeted_post->deleted) {
                                echo '<p class="text-danger">The post has been removed.</p>';
                            } else {
                                echo '<div class="div_pic_name">';
                                echo '<div class="cropper-profile">';
                                echo $this->Html->image(
                                    empty($post->retweeted_post->user->image)
                                        ? 'user.jpg'
                                        : '/img/user/' . h($post->retweeted_post->user->image),
                                    [
                                        'class' => 'pic'
                                    ]
                                );
                                echo '</div>';
                                echo '&nbsp;';
                                echo '<h6>' . h($post->retweeted_post->user->username) . '</h6>';
                                echo '</div>';
                                echo '<p>' . h($post->retweeted_post->post) . '</p>';

                                if ($post->retweeted_post->post_image != null) {
                                    echo '<div class="card bg-secondary">';
                                    echo $this->Html->image(
                                        '/img/post/' . h($post->retweeted_post->post_image),
                                        [
                                            'class' => 'center post_img',
                                            'alt' => h($post->retweeted_post->post)
                                        ]
                                    );
                                    echo '</div>';
                                }
                            } ?>
                        </div>
                    </a>
                <?php } ?>
            </div>
            <div class='row'>
                <div class='col-md-12 col-lg-12 col-sm-12'>
                    <div class='float-left'>
                        <a href='#' class='number-of-likes' data-post='<?= h($post->id) ?>'>
                            <small id='postcount<?= h($post->id) ?>'>
                                <?php
                                if ($post->like_count > 0) {
                                    echo $post->like_count > 1 ?
                                        h($post->like_count) . ' Likes' :
                                        h($post->like_count) . ' Like';
                                }
                                ?>
                            </small>
                        </a>
                    </div>
                    <div class='float-right'>
                        <small>
                            <?php
                            if ($post->comment_count > 0) {
                                echo $this->Html->link(
                                    $post->comment_count > 1 ?
                                        $post->comment_count . ' Comments' :
                                        $post->comment_count . ' Comment',
                                    ['controller' => 'Comments', 'action' => 'seeMore', h($post->id)],
                                );
                            }
                            if ($post->retweet_count > 0) {
                                echo '&nbsp';
                                echo '<span class="text-primary">';
                                echo $post->retweet_count > 1 ? h($post->retweet_count) . ' Retweets' :
                                    h($post->retweet_count) . ' Retweet';
                                echo '</span>';
                            }
                            ?>
                        </small>
                    </div>
                </div>
            </div>
            <div class='d-flex justify-content-between'>
                <div class='float-left'>
                    <?php if (empty($post->likes)) {
                        echo '<a href="#" class="like-post" data-post="' . h($post->id) . '">Like</a>';
                    } else {
                        if ($post->likes[0]->deleted) {
                            echo '<a href="#" class="like-post" data-post="' . h($post->id) . '">Like</a>';
                        } else {
                            echo '<a href="#" class="like-post" data-post="' . h($post->id) . '">Unlike</a>';
                        }
                    } ?>
                </div>
                <div class='float-right'>
                    <div class='dropdown'>
                        <button class='dropbtn'>...</button>
                        <div class='dropdown-content'>
                            <?php
                            if ($me['id'] == $post->user_id) {
                                echo $this->Html->link(
                                    'Edit',
                                    ['controller' => 'posts', 'action' => 'edit', h($post->id)],
                                    ['class' => 'edit_post_link']
                                );
                                echo $this->Form->postLink(
                                    'Delete',
                                    ['controller' => 'posts', 'action' => 'delete', h($post->id)],
                                    [
                                        'confirm' => 'Are you sure to delete?',
                                    ]
                                );
                            }
                            echo $this->Html->link(
                                'Retweet',
                                ['controller' => 'posts', 'action' => 'retweet', h($post->id)],
                                ['class' => 'retweet_post_link']
                            );
                            echo $this->Html->link(
                                'View',
                                ['controller' => 'posts', 'action' => 'view', h($post->id)],
                            );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end-->
    </div>
    <div class='col-md-7 col-lg-7'>
        <div class='div_post'>
            <div id='load_data'></div>
            <div id='load_data_message'></div>
            <div class='div_comment_form'>
                <div class='cropper-comment-form'>
                    <?php echo $this->Html->image(
                        empty($me['image']) ? 'user.jpg' : '/img/user/' . h($me['image']),
                        [
                            'alt' => 'You',
                            'class' => 'pic'
                        ]
                    );
                    ?>
                </div>
                <div>
                    &nbsp;
                    <?php
                    echo $this->Html->link(
                        h($me['username']),
                        ['controller' => 'Users', 'action' => 'userPage', h($me['id'])],
                    );
                    echo $this->Form->create($newComment, [
                        'url' => ['controller' => 'Comments', 'action' => 'add'],
                        'class' => 'save-comment comment-form',
                        'type' => 'post'
                    ]);
                    echo '&nbsp;';
                    echo $this->Form->control(
                        'comment',
                        [
                            'class' => 'form-control',
                            'type' => 'text',
                            'style' => 'width:450px',
                            'label' => false,
                            'required' => false,
                        ]
                    );
                    echo $this->Form->control(
                        'post_id',
                        [
                            'type' => 'hidden',
                            'value' => h($post->id),
                            'id' => 'post_id'
                        ]
                    );
                    echo '&nbsp;';
                    echo $this->Form->button(__('Comment'), [
                        'class' => 'btn-sm btn-success',
                    ]);

                    echo $this->Form->end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    init(false);

    var url = getCommentUrl();
    var data = function(limit, page) {
        return {
            post_id: getPost(),
            limit: limit,
            page: page,
        }
    }
</script>