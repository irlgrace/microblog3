<div class="comment-content" id="page<?= h($page) ?>" style="display:block;">
    <?php foreach ($comments as $comment) { ?>
        <div class="post-comments">
            <?php if ($me['id'] == $comment->user_id) { ?>
                <div class="float-right">
                    <div class="dropleft">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                            ...
                        </a>
                        <div class="dropdown-menu">
                            <?php
                            echo $this->Html->link(
                                'Edit',
                                ['controller' => 'Comments', 'action' => 'edit', h($comment->id)],
                                ['class' => 'edit_comment_link dropdown-item']
                            );
                            echo $this->Form->postLink(
                                'Delete',
                                ['controller' => 'Comments', 'action' => 'delete', h($comment->id)],
                                [
                                    'confirm' => 'Are you sure to delete?',
                                    'class' => 'dropdown-item'
                                ]
                            );
                            ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="div_pic_name">
            <?php
            echo '<div class="cropper-profile">'; 
            echo $this->Html->image(
                empty($comment->user->image) ? 'user.jpg' : '/img/user/' . h($comment->user->image),
                [
                    'class' => 'pic'
                ]
            );
            echo '</div>';
            echo '&nbsp;';
            ?>
            <?= $this->Html->link(
                h($comment->user->username),
                ['controller' => 'Users', 'action' => 'userPage', h($comment->user->id)],
            )
            ?>
            </div>
            <p><?= h($comment->comment) ?></p>
        </div>
    <?php } ?>
</div>