<?php
if (!isset($error)) {
?>
    <?= $this->Form->create($comment, [
        'type' => 'file',
        'url' => ['controller' => 'Comments', 'action' => 'editPost'],
        'class' => 'edit-comment-form'
    ])
    ?>
    <?= $this->Form->control('comment', [
        'label' => false,
        'id' => 'input-post',
        'class' => 'form-control form__inputs',
        'type' => 'textarea',
        'required' => false,
        'templates' => [
            'inputContainer' => '{{content}}'
        ],
    ])
    ?>
    <span class="form__input-error text-danger"></span>

    <div class="float-right">
        <div class="input-group" id="button_post_div">
            <input type="text" id="input-post-count" disabled size="4" />
            &nbsp;
            <?= $this->Form->button(
                _('Save Comment'),
                [
                    'class' => 'btn-sm btn-success',
                ]
            )
            ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
<?php } ?>