<?php if(!isset($error)) {
    foreach ($likers as $liker) {
        echo '<div class="div_post">'.
                '<div class="div_pic_name">'.
                    '<div class="cropper-profile">'.
                    $this->Html->image(
                        empty($liker->user->image)?'user.jpg':'/img/user/'.h($liker->user->image), 
                    [
                        'class' => 'pic'
                    ]).
                    '</div>'.
                    '&nbsp;'.
                    '<div>'.
                    '<h5>'.$this->Html->link(
                        $liker->user->username != $me['username']?
                        h($liker->user->username):
                        h($liker->user->username).' (You)',
                        ['controller' => 'users','action' => 'userPage', h($liker->user->id)],
                    ).'</h5>'.
                    '<small><em>'.h($liker->user->email).'</em></small>'.
                    '</div>'.
                '</div>'.
                '</div>';   
    } 

    if(isset($postId)) {
        echo '<small>';
        echo $this->Html->link(
            'See More Likers...',
            ['controller' => 'likes', 'action' => 'seeMoreLikers', h($postId)],
        );
        echo '</small>';
    }
}
?>