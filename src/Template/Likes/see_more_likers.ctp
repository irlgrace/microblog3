<div id='content' class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class='card'>
            <div class='card-body'>
                <div class='row'>
                    <div class='col-md-12 col-lg-12'>
                        <div class='float-left'>
                            <input type='hidden' id='post_id' value='<?= h($post->id) ?>' />
                            <h4>
                                <?= $post->like_count > 1 ?
                                    h($post->like_count) . ' Likers' :
                                    h($post->like_count) . ' Liker' ?>
                            </h4>
                        </div>
                    </div>
                </div>
                <hr />
                <div class='row'>
                    <div class='col-md-12 col-lg-12'>
                        <div id='load_data'></div>
                        <div id='load_data_message'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //FOR PAGINATION
    var url = getLikerListUrl();
    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
            post_id: getPost()
        }
    }
</script>