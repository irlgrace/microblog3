<?php
    if(!$followings->isEmpty()) {
        echo '<ul class="list-group list-group-flush">';
        foreach ($followings as $following) {
            echo '<li class="list-group-item">' .
                '<div class="div_pic_name">' .
                '<div class="cropper-profile">'.
                $this->Html->image(
                    empty($following->following_user->image) ? 'user.jpg' : '/img/user/'.h($following->following_user->image),
                    [
                        'class' => 'pic'
                    ]
                ) .
                '</div>'.
                '&nbsp;' .
                '<h5>' . $this->Html->link(
                    $following->following_user->username != $me['username']?
                    h($following->following_user->username):
                    h($following->following_user->username).' (You)',
                    ['controller' => 'users', 'action' => 'userPage', h($following->following_user->id)],
                ) . '</h5>' .
                '&nbsp;' .
                '<small><em>' . h($following->following_user->email) . '</em></small>' .
                '</div>' .
                '</li>';
        }
        echo '</ul>';
    }
?>
