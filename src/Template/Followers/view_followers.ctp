<div id='content' class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="float-left">
                            <input type="hidden" id="user_id" value="<?= h($user->id) ?>" />
                            <h4><?php
                                echo $this->Html->link(
                                    'Followers',
                                    [
                                        'controller' => 'followers',
                                        'action' => 'viewFollowers',
                                        h($user->id)
                                    ],
                                    ['style' => 'color:blue']
                                );
                                echo ' | ';
                                echo $this->Html->link(
                                    'Followings',
                                    [
                                        'controller' => 'followers',
                                        'action' => 'viewFollowings',
                                        h($user->id)
                                    ],
                                );
                                ?>
                            </h4>
                        </div>
                        <div class="float-right">
                            <h5><?= $user->follower_count > 1 ?
                                    h($user->follower_count) . ' Followers' :
                                    h($user->follower_count) . ' Follower' ?>
                            </h5>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="<?= $user->follower_count > 0 ? 'card' : '' ?>">
                    <div id="load_data"></div>
                </div>
                <div id="load_data_message"></div>
            </div>
        </div>
    </div>
</div>
<script>
    var url = getFollowerUrl();

    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
            user_id: getUser()
        }
    }
</script>