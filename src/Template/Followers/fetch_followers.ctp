<?php
    if(!$followers->isEmpty()) {
        echo '<ul class="list-group list-group-flush">';
        foreach ($followers as $follower) {
            echo '<li class="list-group-item">' .
                '<div class="div_pic_name">' .
                '<div class="cropper-profile">'.
                $this->Html->image(
                    empty($follower->user->image) ? 'user.jpg' : '/img/user/'.h($follower->user->image),
                    [
                        'class' => 'pic'
                    ]
                ) .
                '</div>'.
                '&nbsp;' .
                '<h5>' . $this->Html->link(
                    $follower->user->username != $me['username']?
                    h($follower->user->username):
                    h($follower->user->username).' (You)',
                    ['controller' => 'users', 'action' => 'userPage', h($follower->user->id)],
                ) . '</h5>' .
                '&nbsp;' .
                '<small><em>' . h($follower->user->email) . '</em></small>' .
                '</div>' .
                '</li>';
        }
        echo '</ul>';
    }
?>
