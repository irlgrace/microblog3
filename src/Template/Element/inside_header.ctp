<nav class="navbar navbar-dark navbar-expand-lg bg-dark justify-content-between fixed-top">
    <div class="container">
        <?php echo $this->Html->link(
            'MICROBLOG 3',
            [
                'controller' => 'Posts',
                'action' => 'index'
            ],
            [
                'class' => 'navbar-brand',
                'style' => 'color:#ffffff;'
            ]
        );
        ?>
        <?php
        echo $this->Form->create(false);
        echo $this->Form->end();
        
        echo $this->Form->create(
            false,
            [
                'url' => ['controller' => 'users', 'action' => 'searchUser'],
                'id' => 'UserSearch',
                'class' => 'form-inline',
                'type' => 'get'
            ],
        );
        echo $this->Form->control('search', [
            'label' => false,
            'class' => 'form-control mr-sm2',
            'type' => 'search',
            'placeholder' => 'Search',
            'value' => isset($search)?h($search):'',
        ]);
        echo '&nbsp;';
        echo $this->Form->button(__('Search'),
        [
            'class' => 'btn btn-primary my-2 my-sm-0',
            'type' => 'submit'
        ]);
        echo $this->Form->end();
        ?>
        
        &nbsp;
        <div class='border border-secondary'>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                        <div class="d-flex justify-content-between">
                        <div class="cropper-profile">
                        <?php echo $this->Html->image(
                            empty($me['image']) ? 'user.jpg' : '/img/user/'.h($me['image']),
                            [
                                'alt' => 'You',
                                'class' => 'pic',
                            ]
                        );
                        echo '</div>';
                        echo '<div>';
                        echo '&nbsp';
                        echo h($me['username']);
                        ?> <span class="navbar-toggler-icon"></span>
                        </div>
                        </div>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <?php
                        echo $this->Html->link(
                            'My UserPage',
                            [
                                'controller' => 'users',
                                'action' => 'userPage',
                                h($me['id'])
                            ],
                            ['class' => 'dropdown-item']
                        );
                        echo $this->Html->link(
                            'Account Settings',
                            [
                                'controller' => 'users',
                                'action' => 'accountSettings',
                            ],
                            ['class' => 'dropdown-item']
                        );
                        echo $this->Html->link(
                            'Logout',
                            [
                                'controller' => 'users',
                                'action' => 'logout',
                            ],
                            ['class' => 'dropdown-item']
                        );
                        ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>