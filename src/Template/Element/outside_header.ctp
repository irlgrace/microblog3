<nav class="navbar navbar-dark navbar-expand-lg bg-dark justify-content-between">
    <div class="container">
        <?php
        echo $this->Html->link(
            'MICROBLOG 3',
            [
                'controller' => 'users',
                'action' => 'register'
            ],
            [
                'class' => 'navbar-brand',
                'style' => 'color:#ffffff;'
            ]
        );

        if (!isset($login)) {
            echo $this->Form->create(null, [
                'url' => ['controller' => 'Users', 'action' => 'login'],
                'class' => 'form-inline'
            ]);
            echo $this->Form->control(
                'username',
                [
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'text',
                    'placeholder' => 'Username',
                    'value' => ''
                ]
            );
        ?>
            &nbsp;
            <?php

            echo $this->Form->control(
                'password',
                [
                    'label' => false,
                    'class' => 'form-control',
                    'type' => 'password',
                    'placeholder' => 'Password',
                    'value' => ''
                ]
            );

            ?>
            &nbsp;
        <?php
            echo $this->Form->button(
                __('Login'),
                [
                    'label' => 'Login',
                    'class' => 'btn btn-primary',
                ]
            );

            echo $this->Form->end();
        }
        ?>
    </div>
</nav>