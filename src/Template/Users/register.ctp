<div class="card shadow cream" style="width: 27rem;">
    <div class="card-body">
        <h5 class="card-title">Register User</h5>
        <?php
        if (isset($userResend)) {
            echo $this->Form->create($userResend, [
                'url' => ['controller' => 'Users', 'action' => 'resendMail'],
                'id' => 'ResendMail'
            ]);
            echo $this->Form->control(
                'username',
                [
                    'type' => 'hidden',
                    'value' => h($userResend['username'])
                ]
            );
            echo $this->Form->control(
                'email',
                [
                    'type' => 'hidden',
                    'value' => h($userResend['email'])
                ]
            );
            echo $this->Form->control(
                'activation_code',
                [
                    'type' => 'hidden',
                    'value' => h($userResend['activation_code'])
                ]
            );
            echo $this->Form->button(
                __('Resend Email Verification'),
                [
                    'label' => 'Resend Email Verification',
                    'class' => 'btn-sm btn-warning',
                ]
            );
            echo $this->Form->end();
        }

        echo $this->Flash->render();
        echo $this->Form->create($newUser);
        echo $this->Form->control(
            'username',
            [
                'label' => 'Username:',
                'class' => 'form-control-custom',
                'type' => 'text',
                'required' => false,
            ]
        );
        echo $this->Form->control(
            'password',
            [
                'label' => 'Password:',
                'class' => 'form-control-custom',
                'type' => 'password',
                'required' => false,
            ]
        );
        echo $this->Form->control(
            'confirm_password',
            [
                'label' => 'Confirm Password:',
                'class' => 'form-control-custom',
                'type' => 'password',
                'required' => false,
            ]
        );
        echo $this->Form->control(
            'email',
            [
                'label' => 'Email:',
                'class' => 'form-control-custom',
                'type' => 'text',
                'required' => false,
            ]
        );
        echo $this->Form->button(
            __('Register'),
            [
                'class' => 'form-control-custom success-custom',
                'type' => 'submit'
            ]
        );
        echo $this->Form->end();

        ?>
    </div>
</div>