<div class="row">
    <div class="col-md-5 col-lg-5">
        <div id="userSide">
            <div class="card text-center bg-secondary per_card" id="me_card">
                <div class="card-body">
                    <div class="image-cropper center" style="margin-top:10%">
                    <?php echo $this->Html->image(empty($user->image) ? 'user.jpg' : '/img/user/' . h($user->image), [
                        'alt' => 'You',
                        'class' => 'profile-pic',
                    ]); ?>
                    </div>
                    <br />
                    <h5 class="card-title">
                        <?php echo $this->Html->link(
                            h($user->username),
                            [
                                'controller' => 'users',
                                'action' => 'userPage', h($user->id)
                            ],
                            [
                                'class' => 'white'
                            ]
                        );
                        ?>
                    </h5>
                    <p class="white"><?= h($user->email) ?></p>
                    <input type="hidden" value="<?= h($user->id) ?>" id="user_id" />
                </div>
            </div>
            <div class="card text-center per_card shadow">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item" id="follower_following_count">
                        <?php
                        echo $user->follower_count > 1 ? h($user->follower_count) . ' Followers ' : h($user->follower_count) . ' Follower ';
                        echo $user->following_count > 1 ? h($user->following_count) . ' Followings' : h($user->following_count) . ' Following';
                        ?>
                    </li>
                </ul>
            </div>
            <div class="card per_card shadow">
                <div class="card-header text-center bg-secondary">
                    <?= $this->Html->link(
                        'List of Followers',
                        [
                            'controller' => 'followers',
                            'action' => 'viewFollowers',
                            h($user->id)
                        ],
                        ['class' => 'white']
                    ) ?>
                </div>
                <ul class="list-group list-group-flush" id="followers_ul">
                    <?php

                    if ($followers->isEmpty()) {
                        echo '<li class="list-group-item text-center">' .
                            'No Followers...' .
                            '</li>';
                    } else {
                        foreach ($followers as $follower) {
                            echo '<li class="list-group-item">';
                            echo '<div class="div_pic_name">';
                            echo '<div class="cropper-profile">';
                            echo $this->Html->image(
                                empty($follower->user->image) ? 'user.jpg' : '/img/user/' . h($follower->user->image),
                                [
                                    'alt' => 'You',
                                    'class' => 'pic',
                                ]
                            );
                            echo '</div>';
                            echo '&nbsp;';
                            echo $this->Html->link(
                                h($follower->user->username),
                                [
                                    'controller' => 'users',
                                    'action' => 'userPage', h($follower->user->id)
                                ],
                            );
                            echo '</div>';
                            echo '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
            <div class="card per_card shadow">
                <div class="card-header text-center bg-secondary">
                    <?= $this->Html->link(
                        'List of Followings',
                        [
                            'controller' => 'followers',
                            'action' => 'viewFollowings',
                            h($user->id)
                        ],
                        ['class' => 'white']
                    ) ?>
                </div>
                <ul class="list-group list-group-flush" id="followers_ul">
                    <?php
                    if ($followings->isEmpty()) {
                        echo '<li class="list-group-item text-center">' .
                            'No Followings...' .
                            '</li>';
                    } else {
                        foreach ($followings as $following) {
                            echo '<li class="list-group-item">';
                            echo '<div class="div_pic_name">';
                            echo '<div class="cropper-profile">';
                            echo $this->Html->image(
                                empty($following->following_user->image) ? 'user.jpg' : '/img/user/' . h($following->following_user->image),
                                [
                                    'alt' => 'You',
                                    'class' => 'pic',
                                ]
                            );
                            echo '</div>';
                            echo '&nbsp;';
                            echo $this->Html->link(
                                h($following->following_user->username),
                                [
                                    'controller' => 'users',
                                    'action' => 'userPage', h($following->following_user->id)
                                ],
                            );
                            echo '</div>';
                            echo '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-7 col-sm-7 col-lg-7">
        <div class="div_follow">
            <?php
            if ($me['id'] != $user->id) {
                if ($user->followed) {
                    echo '<a href="#" class="btn btn-primary follow_btn" data-user="' . h($user->id) . '">Unfollow</a>';
                } else {
                    echo '<a href="#" class="btn btn-primary follow_btn" data-user="' . h($user->id) . '">Follow</a>';
                }
            }
            ?>
        </div>
        <div>
            <?php if ($me['id'] == $user->id) { ?>
                <div id='div_post_input' class='card'>
                    <?= $this->Form->create($newPost, [
                        'url' => ['controller' => 'posts', 'action' => 'create'],
                        'type' => 'file',
                        'class' => 'save-post'
                    ])
                    ?>
                    <div class='input-group'>
                        <?= $this->Form->control('post', [
                            'templates' => [
                                'inputContainer' => '{{content}}'
                            ],
                            'label' => false,
                            'id' => 'input-post',
                            'class' => 'form-control',
                            'type' => 'textarea',
                            'rows' => '2',
                            'required' => false,
                            'placeholder' => 'Say something...'
                        ])
                        ?>
                    </div>
                    <div class='float-left'>
                        <div id='button_post_div'>
                            <?= $this->Form->control('post_image', [
                                'label' => false,
                                'type' => 'file'
                            ])
                            ?>
                        </div>
                    </div>
                    <div class='float-right'>
                        <div class='input-group' id='button_post_div'>
                            <input type='text' id='input-post-count' disabled size='4' />
                            &nbsp;
                            <?= $this->Form->button(
                                __('Post'),
                                [
                                    'label' => 'Post',
                                    'class' => 'btn-sm btn-success'
                                ]
                            )
                            ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div id="load_data"></div>
            <div id="load_data_message"></div>
        </div>
    </div>
</div>
<!--end of content -->
<script>
    initIndex();

    var url = getUserPagePostUrl();
    var data = function(limit, page) {
        return {
            limit: limit,
            page: page,
            user_id: getUser()
        }
    }

    includeFollow();
</script>