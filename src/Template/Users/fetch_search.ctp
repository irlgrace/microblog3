<?php                              
    foreach ($users as $user) {
        echo '<div class="div_post">'.
                '<div class="div_pic_name">'.
                    '<div class="cropper-profile">'.
                    $this->Html->image(
                        empty($user->image)?'user.jpg':'/img/user/'.h($user->image), 
                    [
                        'class' => 'pic'
                    ]).
                    '</div>'.
                    '&nbsp;'.
                    '<h5>'.$this->Html->link(
                        $user->username != $me['username']?
                        h($user->username):
                        h($user->username).' (You)',
                        ['controller' => 'users','action' => 'userPage', h($user->id)],
                    ).'</h5>'.
                    '&nbsp;'.
                    '<small><em>'.h($user->email).'</em></small>'.
                '</div>'.
                '</div>';   
    } 
?>