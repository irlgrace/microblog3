<div id='content' class='row'>
    <div class='col-sm-12 col-md-12 col-lg-12'>
        <div class="card">
            <div class="card-body">
                <h3>MyAccount Settings</h3>
                <hr />
            </div>
            <div class="row" style="margin:40px 0px;">
                <div class="col-md-6 col-lg-6 text-center">
                    <div class="image-cropper center">
                    <?php echo $this->Html->image(empty($me['image']) ? 'user.jpg' : '/img/user/' . h($me['image']), [
                        'alt' => 'You',
                        'class' => 'profile-pic',
                    ]); ?>
                    </div>

                    <br />
                    <div id="editImagePasswordButton">
                        <button style="margin: 10px; width: 200px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#editUserImage">
                            Edit Image
                        </button>
                        <br />
                        <button style="margin: 10px; width: 200px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#editPassword">
                            Change Password
                        </button>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6" style="padding-right:80px;">
                    <h5>Username & Email</h5>
                    <?= $this->Form->create($updateUser, [
                        'url' =>
                        [
                            'controller' => 'users',
                            'action' => 'updateUser'
                        ],
                        'class' => 'save-user'

                    ]);
                    ?>
                    <div class="form-group">
                        <?= $this->Form->control(
                            'username',
                            [
                                'label' => 'Username:',
                                'class' => 'form-control form__input',
                                'type' => 'text',
                                'required' => false,
                                'value' => h($me['username']),
                                'templates' => [
                                    'inputContainer' => '{{content}}'
                                ],
                            ]
                        )
                        ?>
                        <span class="form__input-error text-danger"></span>
                    </div>
                    <div class="form-group">
                        <?= $this->Form->control(
                            'email',
                            [
                                'label' => 'Email:',
                                'class' => 'form-control form__input',
                                'type' => 'text',
                                'required' => false,
                                'value' => h($me['email']),
                                'templates' => [
                                    'inputContainer' => '{{content}}'
                                ],
                            ]
                        )
                        ?>
                        <span class="form__input-error text-danger"></span>
                    </div>
                    <?= $this->Form->button(
                        __('Save Username and Email'),
                        ['class' => 'form-control btn btn-success',]
                    )
                    ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- User Image Modal -->
<div class="modal fade" id="editUserImage" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editUserImageTitle">Edit Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create($updateUser, [
                    'url' => ['controller' => 'users', 'action' => 'updateUser'],
                    'type' => 'file',
                    'class' => 'save-user'
                ])
                ?>
                <div class="form-group">
                    <?= $this->Form->control('image', [
                        'label' => 'User Image :',
                        'class' => 'form-control-file form__input',
                        'type' => 'file',
                        'required' => false,
                        'templates' => [
                            'inputContainer' => '{{content}}'
                        ],
                    ])
                    ?>
                    <span class="form__input-error text-danger"></span>
                </div>
                <?= $this->Form->button(__('Save Image'), [
                    'class' => 'btn btn-success'
                ])
                ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<!-- User Password Modal -->
<div class="modal fade" id="editPassword" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editUserPasswordTitle">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create($updateUser, [
                    'url' =>
                    [
                        'controller' => 'users',
                        'action' => 'updateUser',
                    ],
                    'class' => 'save-user'
                ])
                ?>
                <div class="form-group">
                    <?= $this->Form->control('password', [
                        'label' => 'Password :',
                        'class' => 'form-control form__input',
                        'type' => 'password',
                        'value' => '',
                        'required' => false,
                        'templates' => [
                            'inputContainer' => '{{content}}'
                        ],
                    ])
                    ?>
                    <span class="form__input-error text-danger"></span>
                </div>
                <div class="form-group">
                    <?= $this->Form->control('confirm_password', [
                        'label' => 'Confirm Password :',
                        'class' => 'form-control form__input',
                        'type' => 'password',
                        'required' => false,
                        'templates' => [
                            'inputContainer' => '{{content}}'
                        ],
                    ])
                    ?>
                    <span class="form__input-error text-danger"></span>
                </div>
                <?= $this->Form->button(
                    __('Save Password'),
                    [
                        'class' => 'form-control btn btn-success',
                    ]
                )
                ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script>
    initUser();
</script>