<div class="card shadow cream" style="width: 27rem;">
    <div class="card-body">
        <h5 class="card-title">Login User</h5>
        <?= $this->Flash->render() ?>
        <?= $this->Form->create() ?>
        <?php
        echo $this->Form->control(
            'username',
            [
                'label' => 'Username:',
                'class' => 'form-control-custom',
                'type' => 'text'
            ]
        );

        echo $this->Form->control(
            'password',
            [
                'label' => 'Password:',
                'class' => 'form-control-custom',
                'type' => 'password'
            ]
        );
        echo $this->Form->button(
            __('Login'),
            [
                'class' => 'form-control-custom success-custom',
                'type' => 'submit'
            ]
        ); 
        echo $this->Form->end();
        ?>
    </div>
</div>