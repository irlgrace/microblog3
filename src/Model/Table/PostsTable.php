<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Josegonzalez\Upload\Validation\DefaultValidation;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\RetweetedPostsTable&\Cake\ORM\Association\BelongsTo $RetweetedPosts
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        //Add Upload Behavior
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'post_image' => [
                'fields' => [
                    'dir' => 'post_image_dir',
                    'size' => 'post_image_size',
                    'type' => 'post_image_type',
                ],
                'path' => 'webroot{DS}img{DS}post{DS}',

                'nameCallback' => function (array $data, array $settings) {
                    $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    return md5(uniqid(rand(), true)) . '.' . $ext;
                },
                'keepFilesOnDelete' => false,
            ],
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('RetweetedPost', [
            'className' => 'Posts',
            'foreignKey' => 'retweeted_post_id'
        ]);

        $this->hasMany('Comments', [
            'foreignKey' => 'post_id',
        ])
            ->setConditions(['Comments.deleted' => false]);

        $this->hasMany('Likes', [
            'foreignKey' => 'post_id',
        ]);

        //To get the latest comment in the post
        $this->hasOne('RecentComments', [
            'className' => 'Comments',
            'foreignKey' => false,
            'conditions' => function (QueryExpression $exp, Query $query) {
                $subquery = $query
                    ->connection()
                    ->newQuery()
                    ->select(['Comments.id'])
                    ->from(['Comments' => 'comments'])
                    ->where(['Posts.id = Comments.post_id AND Comments.deleted = false'])
                    ->order(['Comments.created' => 'DESC'])
                    ->limit(1);

                return $exp->add(['RecentComments.id' => $subquery]);
            }

        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('post')
            ->maxLength('post', 140, 'Post should not be more than 140 Characters')
            ->requirePresence('post', 'create')
            ->notEmptyString('post', 'Ops. You forgot to say something.')
            ->add('post', 'custom',  [
                'rule' => function ($value, $context) {
                    if (strlen(trim($value)) == 0) {
                        return false;
                    }
                    return true;
                },
                'message' => 'Ops. You forgot to say something'
            ]);

        $validator->allowEmptyFile('post_image');

        $validator->setProvider('upload', DefaultValidation::class);

        $validator->add('post_image', 'validFileFormat', [
            'rule' => function ($value, $context) {
                $allowedFiles = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];
                $extension = pathinfo($context['data']['post_image']['name'], PATHINFO_EXTENSION);
                return in_array($extension, $allowedFiles);
            },
            'message' => 'Invalid File format. Only .png, .jpg, .jpeg are allowed',
            'provider' => 'upload',
        ]);

        $validator->add('post_image', 'fileBelowMaxSize', [
            'rule' => ['isBelowMaxSize', 2000000],
            'message' => 'This file is too large, 2MB is the max size of the file',
            'provider' => 'upload'
        ]);

        $validator->add('post_image', 'fileCompletedUpload', [
            'rule' => 'isCompletedUpload',
            'message' => 'This file could not be uploaded completely',
            'provider' => 'upload'
        ]);

        $validator->add('post_image', 'UPLOAD_ERR_INI_SIZE', [
            'rule' => function ($value, $context) {
                if ((int) $context['data']['post_image']['error'] === 1) {
                    return false;
                };
                return true;
            },
            'message' => 'This file exceeds the upload max file size'
        ]);


        $validator
            ->dateTime('deleted_date')
            ->allowEmptyDateTime('deleted_date');

        $validator
            ->boolean('deleted')
            ->notEmptyString('deleted');

        $validator
            ->integer('comment_count')
            ->notEmptyString('comment_count');

        $validator
            ->integer('like_count')
            ->notEmptyString('like_count');
        return $validator;
    }

    /**
     * Getting Posts for Pagination 
     * 
     * @param array $containOptions, array $conditions, int $page, int $limit
     * @return array $posts
     */
    public function fetchPosts($containOptions, $conditions, $page, $limit)
    {
        $postQuery = $this->find('all')
            ->contain($containOptions)
            ->order(['Posts.created' => 'DESC'])
            ->where($conditions)
            ->page($page, $limit)
            ->limit($limit);
        $posts = $postQuery->all();

        return $posts;
    }

    /**
     * Get And Validate Post method 
     *   
     * @param string|null $id Post id, int|null $userId, array $containOption.
     * @return \Cake\Http\Response 
     * @throws \Cake\Http\Exception\ForbiddenException | \Cake\Http\Exception\NotFoundException
     */
    public function getAndValidatePost($id = null, $userId = null, $containOption = [])
    {
        if (!$id) {
            throw new \Cake\Http\Exception\NotFoundException('No Post Reference');
        }

        $postQuery = $this->find('all')
            ->contain($containOption)
            ->where(['Posts.id' => (int) $id, 'Posts.deleted' => false]);

        $post = $postQuery->first();

        if (!$post) {
            throw new \Cake\Http\Exception\NotFoundException('Post does not exist');
        }

        if ($userId != null) {
            if ($post->user_id != $userId) {
                throw new \Cake\Http\Exception\ForbiddenException('Request not authorized');
            }
        }

        return $post;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['retweeted_post_id'], 'RetweetedPost'));

        return $rules;
    }
}
