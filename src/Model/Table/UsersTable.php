<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Josegonzalez\Upload\Validation\DefaultValidation;

/**
 * Users Model
 *
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\FollowersTable&\Cake\ORM\Association\HasMany $Followers
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        //For Uploading Image
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'image' => [
                'fields' => [
                    'dir' => 'image_dir',
                    'size' => 'image_size',
                    'type' => 'image_type',
                ],
                'path' => 'webroot{DS}img{DS}user{DS}',

                'nameCallback' => function (array $data, array $settings) {
                    $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                    return md5(uniqid(rand(), true)) . '.' . $ext;
                },
            ],
        ]);

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id',
        ])
            ->setConditions(['Comments.deleted' => false]);

        $this->hasMany('Followers', [
            'foreignKey' => 'following_user_id',
        ])
            ->setConditions(['Followers.deleted' => false]);

        $this->hasMany('Followings', [
            'className' => 'Followers',
            'foreignKey' => 'user_id',
        ])
            ->setConditions(['Followings.deleted' => false]);

        $this->hasMany('Likes', [
            'foreignKey' => 'user_id',
        ])
            ->setConditions(['Likes.deleted' => false]);

        $this->hasMany('Posts', [
            'foreignKey' => 'user_id',
        ])
            ->setConditions(['Posts.deleted' => false]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('username')
            ->maxLength('username', 50, 'Username should not be more than 50 Characters')
            ->requirePresence('username', 'create')
            ->notEmptyString('username', 'Username is required')
            ->add(
                'username',
                'unique',
                [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Username already taken'
                ],
            )
            ->add(
                'username',
                'alphaNumericUnderscoreDot',
                [
                    'rule' => function ($value) {
                        return (bool) preg_match('|^[0-9a-zA-Z_.]*$|', $value);
                    },
                    'message' => 'Invalid Username, may consist alphanumeric, period and underscore only'
                ]
            );

        $validator
            ->email('email', false, 'Invalid Email Format')
            ->requirePresence('email', 'create')
            ->notEmptyString('email', 'Email is required')
            ->add(
                'email',
                'unique',
                [
                    'rule' => 'validateUnique',
                    'provider' => 'table',
                    'message' => 'Email already taken'
                ]
            );


        $validator
            ->scalar('password')
            ->maxLength('password', 255, 'Password should NOT be more than 255 characters')
            ->minLength('password', 8, 'Password should be more than 8 Characters')
            ->requirePresence('password', 'create')
            ->notEmptyString('password', 'Password is required');

        $validator
            ->notEmptyString('confirm_password', 'Confirm Password is required')
            ->add('confirm_password', 'compare', [
                'rule' => ['compareWith', 'password'],
                'message' => 'Password mismatch',
            ]);

        $validator->setProvider('upload', DefaultValidation::class);
        $validator->add('image', 'validFileFormat', [
            'rule' => function ($value, $context) {
                $allowedFiles = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];
                $extension = pathinfo($context['data']['image']['name'], PATHINFO_EXTENSION);
                return in_array($extension, $allowedFiles);
            },
            'message' => 'Invalid File format. Only .png, .jpg, .jpeg are allowed',
            'provider' => 'upload',
        ]);

        $validator->add('image', 'fileBelowMaxSize', [
            'rule' => ['isBelowMaxSize', 2000000],
            'message' => 'This file is too large, 2MB is the max size of the file',
            'provider' => 'upload'
        ]);

        $validator->add('image', 'fileCompletedUpload', [
            'rule' => 'isCompletedUpload',
            'message' => 'This file could not be uploaded completely',
            'provider' => 'upload'
        ]);

        $validator->add('image', 'fileFileUpload', [
            'rule' => 'isFileUpload',
            'message' => 'There was no file found to upload',
            'provider' => 'upload'
        ]);

        $validator->add('image', 'UPLOAD_ERR_INI_SIZE', [
            'rule' => function ($value, $context) {
                if ((int) $context['data']['image']['error'] === 1) {
                    return false;
                };
                return true;
            },
            'message' => 'This file exceeds the upload max file size'
        ]);
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
