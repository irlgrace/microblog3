<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property string $post
 * @property string|null $post_image
 * @property int $user_id
 * @property int|null $retweeted_post_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted_date
 * @property bool $deleted
 * @property int $comment_count
 * @property int $like_count
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\RetweetedPost $retweeted_post
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Like[] $likes
 */
class Post extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'post' => true,
        'post_image' => true,
        'user_id' => true,
        'retweeted_post_id' => true,
        'created' => true,
        'modified' => true,
        'deleted_date' => true,
        'deleted' => true,
        'comment_count' => true,
        'like_count' => true,
        'user' => true,
        'retweeted_post' => true,
        'comments' => true,
        'likes' => true,
    ];
}
