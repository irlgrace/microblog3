<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool 
     */
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        // The add and tags actions are always allowed to logged in users.
        if (in_array($action, ['like', 'likerList', 'seeMoreLikers'])) {
            return true;
        }
    }

    /**
     * Initialize method
     * 
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * UnlockedAction before filter 
     *  
     * @return \Cake\Http\Response|null
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['like', 'likerList']);
    }

    /**
     * Like or unlike a post
     * 
     * @return json response or null
     * @throws \Cake\Http\Exception\NotFoundException|Cake\Http\Exception\MethodNotAllowedException
     */
    public function like()
    {
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {
                $this->autoRender = false;
                if (isset($this->request->getData()['post_id'])) {
                    $post_id = $this->request->getData('post_id');

                    $posts = TableRegistry::getTableLocator()->get('Posts');

                    //Validate the post throws NotFoundException
                    $posts->getAndValidatePost($post_id);

                    $conditions = [
                        'Likes.user_id' => $this->Auth->user('id'),
                        'Likes.post_id' => (int) $post_id
                    ];

                    $saved = false;

                    //Check if the record is existing record
                    if ($this->Likes->exists($conditions)) {
                        $queryLike = $this->Likes->find(
                            'all',
                            [
                                'conditions' => $conditions,
                                'contain' => []
                            ]
                        );

                        $like = $queryLike->first();

                        if ($like->deleted) {
                            $like->deleted = false;
                        } else {
                            $like->deleted = true;
                        }

                        if ($this->Likes->save($like)) {
                            $saved = true;
                        }
                    } else {
                        //Create new record if not existing
                        $like = $this->Likes->newEntity();
                        $like->user_id = $this->Auth->user('id');
                        $like->post_id = (int) $post_id;
                        $like->deleted = false;

                        if ($this->Likes->save($like)) {
                            $saved = true;
                        }
                    }

                    if ($saved) { //if saved successfully
                        $queryLikeCount = $posts->find('all')
                            ->select(['like_count'])
                            ->where(['Posts.id' => (int) $post_id]);

                        $postLikeCount = $queryLikeCount->first();

                        $returnArr = [];
                        $returnArr['like_count'] = $postLikeCount->like_count;
                        $returnArr['deleted'] = $like->deleted;

                        return $this->json($returnArr);
                    }
                }
                return null;
            }
        } catch (NotFoundException $e) {
            return null;
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * Fetch all the Liker list of the specific post
     * 
     * @return ajax view 
     * @throws \Cake\Http\Exception\NotFoundException|Cake\Http\Exception\MethodNotAllowedException
     */
    public function likerList()
    {
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {

                $this->viewBuilder()->setLayout('ajax');

                if (isset($this->request->getData()['post_id'])) {
                    $postId = $this->request->getData('post_id');

                    $posts = TableRegistry::getTableLocator()->get('Posts');
                    //Validate the post
                    $posts->getAndValidatePost($postId);

                    $queryLikes = $this->Likes->find('all')
                        ->contain([
                            'Users' => function (Query $q) {
                                return $q
                                    ->select(['id', 'username', 'image', 'email']);
                            }
                        ])
                        ->order(['Likes.modified' => 'DESC'])
                        ->where(['Likes.post_id' => (int) $postId, 'Likes.deleted' => false]);

                    $setLink = false;

                    //Get the total count of the likes
                    $countLikers = $queryLikes->count();

                    //Check if See More Likers     
                    if (
                        isset($this->request->getData()['limit']) &&
                        isset($this->request->getData()['page'])
                    ) {
                        $limit = $this->request->getData('limit');
                        $page = $this->request->getData('page');
                        $queryLikes->limit($limit);
                        $queryLikes->page($page, $limit);
                    } else {
                        $queryLikes->limit(5);
                        $setLink = true;
                    }

                    $likers = $queryLikes->all();

                    if (count($likers) == 5 && $countLikers > 5 && $setLink) {
                        $this->set(['postId' => $postId]);
                    }

                    $this->set(['likers' => $likers]);
                } else {
                    //if error is set, return blank
                    $this->set(['error' => true]);
                }
            }
        } catch (NotFoundException $e) {
            //if error is set, return blank
            $this->set(['error' => true]);
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     *  Retrieve view for see more likers
     * 
     *  @return \Cake\Http\Response|null
     *  @throws \Cake\Http\Exception\NotFoundException
     *  @param string|null $post_id Post id
     */
    public function seeMoreLikers($post_id = null)
    {
        try {
            $posts = TableRegistry::getTableLocator()->get('Posts');

            //Validate the post
            $post = $posts->getAndValidatePost($post_id);

            $this->viewBuilder()->setLayout('inside');
            $this->set(['post' => $post]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            $this->redirect($this->referer());
        }
    }
}
