<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool 
     */
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        // The add and tags actions are always allowed to logged in users.
        if (in_array(
            $action,
            [
                'follow',
                'viewFollowers',
                'fetchFollowers',
                'viewFollowings',
                'fetchFollowings'
            ]
        )) {
            return true;
        }
    }

    /**
     * Initialize method
     * 
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('inside');
    }

    /**
     * Before filter 
     *  
     * Allowing function for ajax request.
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig(
            'unlockedActions',
            [
                'follow',
                'fetchFollowers',
                'fetchFollowings'
            ]
        );
    }

    /**
     * Follow or unfollow a user
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return json response or null
     */
    public function follow()
    {
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {
                $this->autoRender = false;

                if (isset($this->request->getData()['user_id'])) {
                    $user_id = $this->request->getData('user_id');
                    //Check if user follow itself
                    if ($this->Auth->user('id') == $user_id) {
                        return null;
                    }

                    $users = TableRegistry::getTableLocator()->get('Users');

                    $userCondition = [
                        'Users.id' => (int) $user_id,
                        'Users.deleted' => false,
                        'Users.activated' => true,
                    ];

                    //Check if user is valid
                    if (!$users->exists($userCondition)) {
                        return null;
                    }

                    $followerCondition = [
                        'Followers.user_id' => $this->Auth->user('id'),
                        'Followers.following_user_id' => (int) $user_id
                    ];
                    $saved = false;

                    //Check if there is existing record
                    if ($this->Followers->exists($followerCondition)) {
                        $followerQuery = $this->Followers->find('all')
                            ->where($followerCondition);
                        $follower = $followerQuery->first();

                        if ($follower->deleted) {
                            $follower->deleted = false;
                        } else {
                            $follower->deleted = true;
                        }

                        if ($this->Followers->save($follower)) {
                            $saved = true;
                        }
                    } else {
                        //Create new one if none
                        $follower = $this->Followers->newEntity();
                        $follower->user_id = $this->Auth->user('id');
                        $follower->following_user_id = $user_id;
                        $follower->deleted = false;
                        if ($this->Followers->save($follower)) {
                            $saved = true;
                        }
                    }

                    if ($saved) { //if saved successfully

                        //Getting updated follower and following count of users to follow
                        $userQuery = $users->find('all', [
                            'conditions' => $userCondition,
                        ]);
                        $user = $userQuery->first();
                        $follower->follower_count = $user->follower_count;
                        $follower->following_count = $user->following_count;

                        //Getting the updated list of first 5 recent follower
                        $followerListQuery = $this->Followers->find('all')
                            ->contain(['Users' => function (Query $q) {
                                return $q->select(['image', 'username', 'id']);
                            }])
                            ->where(
                                [
                                    'Followers.deleted' => false,
                                    'Followers.following_user_id' => (int) $user_id
                                ]
                            )
                            ->order(['Followers.modified' => 'DESC'])
                            ->limit(5);
                        $followerList = $followerListQuery->all();

                        $follower->followerList = $followerList;

                        return $this->json($follower);
                    }
                }
                return null;
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * View of List of Follower
     * 
     * @return view or redirected page
     * @throws \Cake\Http\Exception\NotFoundException
     * @param string|null $id User id
     */
    public function viewFollowers($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No user reference'));
            }

            $users = TableRegistry::getTableLocator()->get('Users');

            $userCondition = [
                'Users.id' => (int) $id,
                'Users.deleted' => false
            ];

            if (!$users->exists($userCondition)) {
                throw new NotFoundException(__('Invalid User'));
            }
            $userQuery = $users->find('all')
                ->where($userCondition);

            $user = $userQuery->first();

            $this->set(['user' => $user]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect($this->referer());
        }
    }

    /**
     * Function for Retrieving List of Follower
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return ajax view
     */
    public function fetchFollowers()
    {
        try {
            $this->viewBuilder()->setLayout('ajax');
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {
                if (
                    isset($this->request->getData()['limit']) &&
                    isset($this->request->getData()['page']) &&
                    isset($this->request->getData()['user_id'])
                ) {
                    $limit = $this->request->getData('limit');
                    $page = $this->request->getData('page');
                    $userId = $this->request->getData('user_id');

                    $followerListQuery = $this->Followers->find('all')
                        ->contain(['Users' => function (Query $q) {
                            return $q->select(['image', 'username', 'id', 'email']);
                        }])
                        ->where(
                            [
                                'Followers.deleted' => false,
                                'Followers.following_user_id' => (int) $userId
                            ]
                        )
                        ->order(['Users.username' => 'ASC'])
                        ->limit($limit)
                        ->page($page, $limit);

                    $followers = $followerListQuery->all();

                    $this->set(['followers' => $followers]);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * View of List of Following
     * 
     * @return view or redirected page
     * @throws \Cake\Http\Exception\NotFoundException
     * @param string|null $id User id
     */
    public function viewFollowings($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No user reference'));
            }

            $users = TableRegistry::getTableLocator()->get('Users');

            $userCondition = [
                'Users.id' => (int) $id,
                'Users.deleted' => false
            ];

            if (!$users->exists($userCondition)) {
                throw new NotFoundException(__('Invalid User'));
            }
            $userQuery = $users->find('all')
                ->where($userCondition);

            $user = $userQuery->first();

            $this->set(['user' => $user]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect($this->referer());
        }
    }

    /**
     * Function for Retrieving List of Followings
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return ajax view
     */
    public function fetchFollowings()
    {
        try {
            $this->viewBuilder()->setLayout('ajax');
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {
                if (
                    isset($this->request->getData()['limit']) &&
                    isset($this->request->getData()['page']) &&
                    isset($this->request->getData()['user_id'])
                ) {
                    $limit = $this->request->getData('limit');
                    $page = $this->request->getData('page');
                    $userId = $this->request->getData('user_id');

                    $followingListQuery = $this->Followers->find('all')
                        ->contain(['FollowingUsers' => function (Query $q) {
                            return $q->select(['image', 'username', 'id', 'email']);
                        }])
                        ->where(
                            [
                                'Followers.deleted' => false,
                                'Followers.user_id' => (int) $userId
                            ]
                        )
                        ->order(['FollowingUsers.username' => 'ASC'])
                        ->limit($limit)
                        ->page($page, $limit);

                    $followings = $followingListQuery->all();

                    $this->set(['followings' => $followings]);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }
}
