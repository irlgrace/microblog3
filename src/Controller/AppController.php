<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Controller\Exception\SecurityException;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Posts',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'register',
            ],
            'unauthorizedRedirect' => $this->referer(),

        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        $this->loadComponent('Security');

        if ($this->Auth->user()) {
            //To access Authenticated User in View through me variable
            $this->set('me', $this->Auth->user());
        }
        $this->set('title', 'Microblog 3');
    }

    /**
     * Set config in security component before filter
     *
     * @param \Cake\Event\Event $event 
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('blackHoleCallback', 'blackhole');
    }


    /**
     * Set Layout before render
     *
     * @param Event $event 
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
    }

    /**
     * Blackhole method for security purposes
     * 
     * @return CakeResponse
     */
    public function blackhole($type, SecurityException $exception)
    {
        if ($exception->getMessage() === 'Request is not SSL and the action is required to be secure') {
            // Reword the exception message with a translatable string.
            $exception->setMessage(__('Please access the requested page through HTTPS'));
        }

        // Re-throw the conditionally reworded exception.
        // Alternatively, handle the error, e.g. set a flash message &
        // redirect to HTTPS version of the requested page.
        $msg = $exception->getMessage();

        //For debugging purpose showing error
        $this->Flash->error('Stop Illegal action! ' . $msg, ['escape' => false]);

        //$this->Flash->error('Stop Illegal action! ');

        //Handling Blackhole depending to Request
        if ($this->request->getParam('isAjax')) {
            throw $exception;
        } else {
            return $this->redirect($this->referer());
        }
    }

    /**
     * Set default authorization
     * @param array $user 
     * @return bool 
     */
    public function isAuthorized($user)
    {
        // By default deny access.
        return false;
    }

    /**
     * Json Parse Method
     * 
     * @param array $obj
     * @return json response
     */
    public function json($obj)
    {
        return $this->response->withType('application/json')
            ->withStringBody(json_encode($obj));
    }
}
