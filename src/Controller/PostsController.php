<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Routing\Router;
use Cake\Http\ServerRequest;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool 
     */
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        // The add and tags actions are always allowed to logged in users.
        if (in_array(
            $action,
            [
                'index',
                'create',
                'fetchPost',
                'delete',
                'retweet',
                'retweetPost',
                'view',
                'searchPost',
                'fetchSearch',
                'edit',
                'editPost'
            ]
        )) {
            return true;
        }
        try {
            // All other actions require a id.
            if (in_array($action, ['delete'])) {
                $id = $this->request->getParam('pass.0');
                if (!$id) {
                    return false;
                }
                // Check that the article belongs to the current user.
                $post = $this->Posts->get($id);
                return $post->user_id === $user['id'];
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Initialize method
     * 
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('inside');
    }

    /**
     * Before filter 
     *  
     * Allowing function for ajax request.
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['fetchPost', 'fetchSearch']);
    }

    /**
     * Create method
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return \Cake\Http\Response|null
     */
    public function create()
    {
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            $this->autoRender = false;
            $post = $this->Posts->newEntity();
            if ($this->request->is(['post'])) {
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $post->user_id = $this->Auth->user('id');
                if ($this->Posts->save($post)) {
                    $returnMessage['success'] = 'Successfully saved post!';
                    $this->Flash->success(__($returnMessage['success']));
                    return $this->json($returnMessage);
                } else {
                    $returnMessage['errorList'] = $post->errors();
                    $returnMessage['error'] = 'Unable to save post.';
                    return $this->json($returnMessage);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        } catch (\Exception $e) {
            $this->Flash->error(__($e->getMessage()));
            return null;
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $newPost = $this->Posts->newEntity();
        $this->set(['newPost' => $newPost]);
    }

    /**
     * Function that will render post through ajax  
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return ajax view
     */
    public function fetchPost()
    {
        try {
            $this->viewBuilder()->setLayout('ajax');
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {
                if (
                    isset($this->request->getData()['limit']) &&
                    isset($this->request->getData()['page'])
                ) {
                    $limit = $this->request->getData('limit');
                    $page = $this->request->getData('page');

                    //Getting the Followed User
                    $followers = TableRegistry::getTableLocator()->get('Followers');
                    $followedUserQuery = $followers->find('all', [
                        'conditions' => [
                            'Followers.deleted' => false,
                            'Followers.user_id' => $this->Auth->user('id')
                        ]
                    ]);

                    $followedUser = $followedUserQuery->all();

                    //Adding to Condition the followed user
                    $conditions = [];
                    foreach ($followedUser as $followed) {
                        $conditions[] = ['Posts.user_id' => $followed->following_user_id];
                    }

                    //Add Authenticated User
                    $conditions[] =  ['Posts.user_id' => $this->Auth->user('id')];

                    //Setting the final condition for retrieving post
                    $finalConditions = [
                        'Posts.deleted' => false,
                        'OR' => $conditions
                    ];

                    $contains = [
                        'RecentComments',
                        'RecentComments.Users' => function (Query $q) {
                            return $q
                                ->select(['id', 'username', 'image']);
                        },
                        'Users' => function (Query $q) {
                            return $q
                                ->select(['id', 'username', 'image']);
                        },
                        'RetweetedPost' => function (Query $q) {
                            return $q
                                ->select(['id', 'post', 'deleted', 'post_image', 'user_id']);
                        },
                        'RetweetedPost.Users' => function (Query $q) {
                            return $q
                                ->select(['id', 'username', 'image']);
                        },
                        'Likes' => function (Query $q) {
                            return $q
                                ->where(['Likes.user_id' => $this->Auth->user('id')]);
                        }
                    ];

                    //Fetching Posts
                    $posts = $this->Posts->fetchPosts($contains, $finalConditions, $page, $limit);

                    $comments = TableRegistry::getTableLocator()->get('Comments');
                    $newComment = $comments->newEntity();
                    $this->set(['posts' => $posts, 'page' => $page, 'newComment' => $newComment]);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\NotFoundException
     */
    public function view($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No Post Reference'));
            }
            $conditions = [
                'Posts.deleted' => false,
                'Posts.id' => (int) $id
            ];
            $postQuery = $this->Posts->find('all')
                ->contain([
                    'RecentComments',
                    'RecentComments.Users' => function (Query $q) {
                        return $q
                            ->select(['id', 'username', 'image', 'deleted']);
                    },
                    'Users' => function (Query $q) {
                        return $q
                            ->select(['id', 'username', 'image', 'deleted']);
                    },
                    'RetweetedPost' => function (Query $q) {
                        return $q
                            ->select(['id', 'post', 'deleted', 'post_image', 'user_id']);
                    },
                    'RetweetedPost.Users' => function (Query $q) {
                        return $q
                            ->select(['id', 'username', 'image']);
                    },
                    'Likes' => function (Query $q) {
                        return $q
                            ->where(['Likes.user_id' => $this->Auth->user('id')]);
                    }
                ])
                ->where($conditions);
            $post = $postQuery->first();

            if (!$post) {
                throw new NotFoundException(__('Post does not exist.'));
            }

            $comments = TableRegistry::getTableLocator()->get('Comments');
            $newComment = $comments->newEntity();

            $this->set(['post' => $post, 'newComment' => $newComment]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect($this->referer());
        }
    }

    /**
     * Retrieve edit post form
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null 
     * @throws \Cake\Http\Exception\ForbiddenException | \Cake\Http\Exception\NotFoundException 
     * Cake\Http\Exception\MethodNotAllowedException
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            //return and validate post
            $post = $this->Posts->getAndValidatePost($id, $this->Auth->user('id'));

            $this->set(['post' => $post]);
        } catch (NotFoundException | ForbiddenException $e) {
            $this->Flash->error(__($e->getMessage()));
            //If error is set return null
            $this->set(['error' => $e->getMessage()]);
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * Edit Method when request is post
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null 
     * @throws \Cake\Http\Exception\ForbiddenException | \Cake\Http\Exception\NotFoundException
     * Cake\Http\Exception\MethodNotAllowedException
     */
    public function editPost($id = null)
    {
        try {
            $this->autoRender = false;
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }

            if (!$id) {
                throw new NotFoundException(__('Post does not exists'));
            }

            $postQuery = $this->Posts->find('all')
                ->where(['Posts.id' => (int) $id, 'Posts.deleted' => false]);
            $post = $postQuery->first();

            if (!$post) {
                throw new NotFoundException(__('Post does not exists'));
            } else if ($post->user_id != $this->Auth->user('id')) {
                throw new ForbiddenException(__('Request not authorized'));
            }

            if ($this->request->is(['patch', 'post', 'put'])) {

                $post = $this->Posts->patchEntity($post, $this->request->getData());
                //Remove Image
                if (
                    isset($this->request->getData()['remove_image']) &&
                    $this->request->getData()['remove_image'] == true
                ) {
                    $post->post_image = null;
                    //To able to save null value in posts remove behavior for saving image
                    if ($this->Posts->behaviors()->has('Upload')) {
                        $this->Posts->removeBehavior('Upload');
                    }
                }

                if ($this->Posts->save($post)) {
                    $returnMessage['success'] = 'Successfully saved post!';
                    $this->Flash->success(__($returnMessage['success']));
                    return $this->json($returnMessage);
                } else {
                    $returnMessage['errorList'] = $post->errors();
                    $returnMessage['error'] = 'Unable to save post.';
                    return $this->json($returnMessage);
                }
            }
        } catch (NotFoundException | ForbiddenException $e) {
            $this->Flash->error(__($e->getMessage()));
            return null;
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }


    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null 
     * @throws \Cake\Http\Exception\NotFoundException
     */
    public function delete($id = null)
    {
        try {
            $this->request->allowMethod(['post', 'delete']);

            $postQuery = $this->Posts->find('all')
                ->where(['Posts.id' => (int) $id, 'Posts.deleted' => false]);
            $post = $postQuery->first();

            if (!$post) {
                throw new NotFoundException(__('Post does not exist'));
            }

            $post->deleted = true;
            $post->deleted_date = date('Y-m-d H:i:s');

            if ($this->Posts->save($post)) {
                $this->Flash->success(__('Successfully deleted the post!'));
            } else {
                $this->Flash->error(__('The post could not be deleted. Please, try again.'));
            }
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
        } finally {
            $req = new ServerRequest(['url' => $this->request->referer(true)]);
            $url = Router::parseRequest($req);
            //These routes will not accept deleted post causing redirect infinitely
            //To avoid it redirect to index page
            if (($url['controller'] == 'Posts' && $url['action'] == 'view') ||
                ($url['controller'] == 'Comments' && $url['action'] == 'seeMore')
            ) {
                return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
            }
            return $this->redirect($this->referer());
        }
    }

    /**
     * Retrieve retweet form
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Http\Exception\NotFoundException | Cake\Http\Exception\MethodNotAllowedException
     */
    public function retweet($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            //return post and validate
            $post = $this->Posts->getAndValidatePost($id, null, ['Users']);

            $retweetPost = $this->Posts->newEntity();

            $this->set(['post' => $post, 'retweetPost' => $retweetPost]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            //If error is set return null
            $this->set(['error' => $e->getMessage()]);
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * Retweet when request is post
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return \Cake\Http\Response|null 
     */
    public function retweetPost()
    {
        try {
            $this->autoRender = false;
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            $returnMessage = [];
            $retweetPost = $this->Posts->newEntity();
            if ($this->request->is('post')) {
                $retweetPost = $this->Posts->patchEntity($retweetPost, $this->request->getData());
                $retweetPost->user_id = $this->Auth->user('id');
                if ($this->Posts->save($retweetPost)) {
                    //Add Retweet count to reference post
                    $refPostId = $this->request->getData('ref_post_id');
                    $refPost = $this->Posts->get($refPostId, ['contain' => []]);
                    $refPost->retweet_count = $refPost->retweet_count + 1;
                    $this->Posts->save($refPost);

                    $returnMessage['success'] = 'Successfully saved post!';
                    $this->Flash->success(__($returnMessage['success']));
                    return $this->json($returnMessage);
                } else {
                    $returnMessage['errorList'] = $retweetPost->errors();
                    $returnMessage['error'] = 'Unable to save post.';
                    return $this->json($returnMessage);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * Retrieving Search View
     * 
     * @return view
     */
    public function searchPost()
    {
        $this->viewBuilder()->setLayout('inside');
        if ($this->request->is(['get'])) {
            $searchWord = $this->request->getQuery('search');
            $this->set(['search' => $searchWord]);
        }
    }

    /**
     * Retrieving the data for Search 
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return ajax view
     */
    public function fetchSearch()
    {
        try {
            $this->viewBuilder()->setLayout('ajax');
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {
                if (
                    isset($this->request->getData()['limit']) &&
                    isset($this->request->getData()['page']) &&
                    isset($this->request->getData()['search'])
                ) {
                    $limit = $this->request->getData('limit');
                    $page = $this->request->getData('page');
                    $search = $this->request->getData('search');

                    $conditions = [
                        'OR' => [
                            [
                                'Posts.post LIKE' => '%' . h($search) . '%',
                                'Posts.deleted' => false,
                            ],
                            [
                                'RetweetedPost.post LIKE' => '%' . h($search) . '%',
                                'RetweetedPost.deleted' => false,
                            ]
                        ],
                        'Posts.deleted' => false,
                        'Users.deleted' => false,
                    ];
                    $contains = [
                        'Users' => function (Query $q) {
                            return $q
                                ->select(['id', 'username', 'image']);
                        },
                        'RetweetedPost' => function (Query $q) {
                            return $q
                                ->select(['id', 'post', 'deleted', 'post_image', 'user_id']);
                        },
                        'RetweetedPost.Users',
                    ];

                    //Fetching posts
                    $posts = $this->Posts->fetchPosts($contains, $conditions, $page, $limit);

                    $this->set(['posts' => $posts, 'page' => $page]);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }
}
