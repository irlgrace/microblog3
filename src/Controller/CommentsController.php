<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use Cake\Event\Event;
use Cake\Http\Exception\MethodNotAllowedException;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    /**
     * Set authorization
     * @param $user 
     * @return bool 
     */
    public function isAuthorized($user)
    {
        $action = $this->request->getParam('action');
        // The add and tags actions are always allowed to logged in users.
        if (in_array(
            $action,
            [
                'add',
                'seeMore',
                'fetchComment',
                'edit',
                'editPost'
            ]
        )) {
            return true;
        }
        try {
            // All other actions require a id.
            if (in_array($action, ['delete'])) {
                $id = $this->request->getParam('pass.0');
                if (!$id) {
                    return false;
                }
                // Check that the article belongs to the current user.
                $comment = $this->Comments->get($id);
                return $comment->user_id === $user['id'];
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Initialize method
     * 
     * @return \Cake\Http\Response|null
     */
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('inside');
    }

    /**
     * Before filter 
     *  
     * Allowing function for ajax request.
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Security->setConfig('unlockedActions', ['fetchComment']);
    }

    /**
     * Add method
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return \Cake\Http\Response|null 
     */
    public function add()
    {
        try {
            $this->autoRender = false;
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            $comment = $this->Comments->newEntity();
            if ($this->request->is('post')) {
                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                $comment->user_id = $this->Auth->user('id');
                if ($this->Comments->save($comment)) {
                    $returnMessage['success'] = 'Successfully saved comment!';
                    $this->Flash->success(__($returnMessage['success']));
                    return $this->json($returnMessage);
                } else {
                    $returnMessage['errorList'] = $comment->errors();
                    $returnMessage['error'] = 'Unable to save comment.';
                    return $this->json($returnMessage);
                }
            }
        } catch (\Exception $e) {
            $this->Flash->success($e->getMessage());
            return null;
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * Retrieve edit comment form
     *
     * @param string|null $id Comment id.
     * @return ajax view|null 
     * @throws \Cake\Http\Exception\ForbiddenException | \Cake\Http\Exception\NotFoundException
     * Cake\Http\Exception\MethodNotAllowedException
     */
    public function edit($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if (!$id) {
                throw new NotFoundException(__('No comment referencing'));
            }

            $commentQuery = $this->Comments->find('all')
                ->where(['Comments.id' => (int) $id, 'Comments.deleted' => false]);

            $comment = $commentQuery->first();

            if (!$comment) {
                throw new NotFoundException(__('Comment does not exist'));
            } else if ($comment->user_id != $this->Auth->user('id')) {
                throw new ForbiddenException(__('Request not authorized'));
            }

            //Check if post is deleted
            $posts = TableRegistry::getTableLocator()->get('Posts');

            if ($posts->exists(['Posts.id' => $comment->post_id, 'Posts.deleted' => true])) {
                throw new NotFoundException(__('Comment does not exist'));
            }

            $this->set(['comment' => $comment]);
        } catch (NotFoundException | ForbiddenException $e) {
            $this->Flash->error(__($e->getMessage()));
            //If error is set return null
            $this->set(['error' => $e->getMessage()]);
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }

    /**
     * Edit Method when request is post
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @param string|null $id Post id.
     * @return json response|null 
     */
    public function editPost($id = null)
    {
        try {
            $this->autoRender = false;
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }

            if (!$id) {
                throw new NotFoundException(__('No comment referencing'));
            }

            $commentQuery = $this->Comments->find('all')
                ->where(['Comments.id' => (int) $id, 'Comments.deleted' => false]);
            $comment = $commentQuery->first();

            if (!$comment) {
                throw new NotFoundException(__('Comment does not exists'));
            } else if ($comment->user_id != $this->Auth->user('id')) {
                throw new ForbiddenException(__('Request not authorized'));
            }

            //Check if post is deleted
            $posts = TableRegistry::getTableLocator()->get('Posts');

            if ($posts->exists(['Posts.id' => $comment->post_id, 'Posts.deleted' => true])) {
                throw new NotFoundException(__('Comment does not exist'));
            }

            if ($this->request->is(['patch', 'post', 'put'])) {

                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                if ($this->Comments->save($comment)) {
                    $returnMessage['success'] = 'Successfully saved comment!';
                    $this->Flash->success(__($returnMessage['success']));
                    return $this->json($returnMessage);
                } else {
                    $returnMessage['errorList'] = $comment->errors();
                    $returnMessage['error'] = 'Unable to save comment.';
                    return $this->json($returnMessage);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        } catch (\Exception $e) {
            $this->Flash->error(__($e->getMessage()));
            return null;
        }
    }

    /**
     * Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\NotFoundException 
     */
    public function delete($id = null)
    {
        try {
            $this->request->allowMethod(['post', 'delete']);

            $commentQuery = $this->Comments->find('all')
                ->where(['Comments.id' => (int) $id, 'Comments.deleted' => false]);

            $comment = $commentQuery->first();

            if (!$comment) {
                throw new NotFoundException(__('Comment does not exist'));
            }

            //Check if post is deleted
            $posts = TableRegistry::getTableLocator()->get('Posts');

            if ($posts->exists(['Posts.id' => $comment->post_id, 'Posts.deleted' => true])) {
                throw new NotFoundException(__('Comment does not exist'));
            }

            $comment->deleted = true;
            $comment->deleted_date = date('Y-m-d H:i:s');

            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('Successfully deleted the comment!'));
            } else {
                $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
            }
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
        } finally {
            return $this->redirect($this->referer());
        }
    }

    /**
     * Viewing all Comments for specific post
     * 
     * @return view or redirected page
     * @throws \Cake\Http\Exception\NotFoundException 
     * @param string|null $id Post id
     */
    public function seeMore($id = null)
    {
        try {
            if (!$id) {
                throw new NotFoundException(__('No post referencing.'));
            }

            $posts = TableRegistry::getTableLocator()->get('Posts');
            $postQuery = $posts->find('all', [
                'contain' => [
                    'Users' => function (Query $q) {
                        return $q
                            ->select(['id', 'username', 'image']);
                    },
                    'RetweetedPost' => function (Query $q) {
                        return $q
                            ->select(['id', 'post', 'deleted', 'post_image', 'user_id']);
                    },
                    'RetweetedPost.Users' => function (Query $q) {
                        return $q
                            ->select(['id', 'username', 'image']);
                    },
                    'Likes' => function (Query $q) {
                        return $q
                            ->where(['Likes.user_id' => $this->Auth->user('id')]);
                    }
                ],
            ])->where(['Posts.id' => (int) $id, 'Posts.deleted' => false]);

            $post = $postQuery->first();

            if (!$post) {
                throw new NotFoundException(__('Post does not exist.'));
            }

            $newComment = $this->Comments->newEntity();

            $this->set(['newComment' => $newComment, 'post' => $post]);
        } catch (NotFoundException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect($this->referer());
        }
    }

    /**
     * Retrieving the comments for specific Post 
     * @throws Cake\Http\Exception\MethodNotAllowedException
     * @return ajax view
     */
    public function fetchComment()
    {
        $this->viewBuilder()->setLayout('ajax');
        try {
            if (!$this->request->is('ajax')) {
                throw new MethodNotAllowedException('Not allowed to access');
            }
            if ($this->request->is(['post'])) {
                if (
                    isset($this->request->getData()['limit']) &&
                    isset($this->request->getData()['page']) &&
                    isset($this->request->getData()['post_id'])
                ) {

                    $postId = $this->request->getData('post_id');
                    $limit = $this->request->getData('limit');
                    $page = $this->request->getData('page');

                    $conditions = [
                        'Comments.deleted' => false,
                        'Comments.post_id' => $postId
                    ];

                    $contains = [
                        'Users' => function (Query $q) {
                            return $q
                                ->select(['id', 'username', 'image']);
                        }
                    ];

                    //Fetch Comments
                    $comments = $this->Comments->fetchComments($contains, $conditions, $page, $limit);
                    $this->set(['comments' => $comments, 'page' => $page]);
                }
            }
        } catch (MethodNotAllowedException $e) {
            $this->Flash->error(__($e->getMessage()));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }
}
